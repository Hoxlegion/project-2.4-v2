/*
    Author: Timo de Jong
    Version: 1.0
    Description: Document for making the connection to the database
**/

const Sequelize = require("sequelize")
const sequelize = new Sequelize("etenstijd", "root", "", {
    host: "localhost",
    dialect: "mysql",
    operatorsAliases: false,

    pool: {
        aquire: 30000,
        idle: 10000
    }
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

module.exports = db

