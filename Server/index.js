/*
    Author: Timo de Jong
    Version: 1.0
    Description: Listens to the port for incomming requests
**/

const express = require('express');
const cors = require('cors')
const fs = require('fs'); 
const _ = require("lodash");
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
const routes = require('./routes/routes')
var app = new express();

const expressJwt = require('express-jwt');

app.use(bodyParser.json());
app.use(cors())
app.use(bodyParser.urlencoded({
  extended: true
}));

const PORT = process.env.PORT || 5000;

app.use('/', routes)

app.listen(PORT, () => {
  console.log("Live on "+PORT)
  console.log("Express running")
});
