/*
    Author: Timo de Jong
    Version: 1.0
    Description: Model for the chain table
**/

const Sequelize = require("sequelize")
const db = require("../database/db.js")

module.exports = db.sequelize.define(
    'chain',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING
        },
        streetname: {
            type: Sequelize.STRING
        },
        streetnumber: {
            type: Sequelize.INTEGER
        },
        zipcode: {
            type: Sequelize.STRING
        },
        city: {
            type: Sequelize.STRING
        },
        phonenumber: {
            type: Sequelize.INTEGER
        },
        created: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW
        }
    },
    {
        timestamps: false
    }
)