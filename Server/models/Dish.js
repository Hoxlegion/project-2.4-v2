/*
    Author: Timo de Jong
    Version: 1.0
    Description: Model for the dish table
**/

const Sequelize = require("sequelize")
const db = require("../database/db.js")

module.exports = db.sequelize.define(
    'dish',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        restaurantId: {
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        price: {
            type: Sequelize.DECIMAL
        },
        created: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW
        },
        course: {
            type: Sequelize.STRING
        },
        allergies: {
            type: Sequelize.STRING
        }
    },
    {
        timestamps: false
    }
)