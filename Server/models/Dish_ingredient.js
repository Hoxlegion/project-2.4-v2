/*
    Author: Timo de Jong
    Version: 1.0
    Description: Model for the dish_ingredient table
**/

const Sequelize = require("sequelize")
const db = require("../database/db.js")

module.exports = db.sequelize.define(
    'dish_ingredient',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        dishId: {
            type: Sequelize.INTEGER
        },
        ingredientId: {
            type: Sequelize.INTEGER
        }
    },
    {
        timestamps: false
    }
)