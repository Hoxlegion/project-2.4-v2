/*
    Author: Timo de Jong
    Version: 1.0
    Description: Model for the order table
**/

const Sequelize = require("sequelize")
const db = require("../database/db.js")

module.exports = db.sequelize.define(
    'order',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        restaurantId: {
            type: Sequelize.INTEGER
        },
        dishIds: {
            type: Sequelize.STRING
        },
        userId: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.INTEGER
        },
        paymentMethodId: {
            type: Sequelize.INTEGER
        },
        price: {
            type: Sequelize.STRING
        },
        deliveryAddressStreet: {
            type: Sequelize.STRING
        },
        deliveryAddressNumber: {
            type: Sequelize.INTEGER
        },
        deliveryAddressZipcode: {
            type: Sequelize.STRING
        },
        deliveryAddressCity: {
            type: Sequelize.STRING
        },
        deliveryTime: { 
            type: Sequelize.STRING
        },
        created: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW
        },
        phone: {
            type: Sequelize.INTEGER
        },
        name: { 
            type: Sequelize.STRING
        }, 
        businessName: { 
            type: Sequelize.STRING
        },
        comments: { 
            type: Sequelize.STRING
        },
        isDelivered: {
            type: Sequelize.INTEGER
        },
    },
    {
        timestamps: false
    }
)