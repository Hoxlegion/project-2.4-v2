/*
    Author: Timo de Jong
    Version: 1.0
    Description: Model for the restaurant table
**/

const Sequelize = require("sequelize")
const db = require("../database/db.js")


module.exports = db.sequelize.define(
    'restaurant',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        chainId: {
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        streetname: {
            type: Sequelize.STRING
        },
        streetnumber: {
            type: Sequelize.INTEGER
        },
        zipcode: {
            type: Sequelize.STRING
        },
        city: {
            type: Sequelize.STRING
        },
        phonenumber: {
            type: Sequelize.INTEGER
        },
        minOrderPrice: {
            type: Sequelize.DECIMAL
        },
        deliveryCosts: {
            type: Sequelize.DECIMAL
        },
        deliveryZipcodes: {
            type: Sequelize.STRING
        },
        diets: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        created: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW
        }
    },
    {
        timestamps: false
    }    
)
