/*
    Author: Timo de Jong
    Version: 1.0
    Description: Model for the restaurant_categorie table
**/

const Sequelize = require("sequelize")
const db = require("../database/db.js")

module.exports = db.sequelize.define(
    'Restaurant_catagorie',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        restaurantId: {
            type: Sequelize.INTEGER,
            foreignKey: true,
            as: 'restaurant_catagorie'
        },
        categorieId: {
            type: Sequelize.STRING,
            foreignKey: true
        }
    },
    {
        timestamps: false,

    }
)
