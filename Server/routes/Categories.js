/*
    Author: Timo de Jong
    Version: 1.0
    Description: Document containing all the query functions for interactions with the categorie table
**/

const express = require("express")
const categories = express.Router()
const cors = require("cors")

const Categorie = require("../models/Categorie")
categories.use(cors())

categories.post('/add', (req, res) => {
    let data = setData(req)

    Categorie.findOne({
        where: {
            name: req.body.name
        }
    })
        .then(categorie => {
            if(!categorie) {
                Categorie.create(data)
                .then(Categorie => {
                    res.send('Gelukt')
                })
                .catch(err => {
                    res.send('error' + err)
                })
            } else {
                res.json({ err: 'categorie already exists' })
            }
        })
        .catch(err => {
            res.send('error:' + err)
        })
})

categories.get('/get', (req, res) => {

  Categorie.findOne({
        where: {
            id: req.body.id
        }
    })
    .then(categorie => {
        if(categorie) {
            res.json(categorie)
        } else {
            res.send('Categorie does not exist')
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

categories.get('/getAll', (req, res) => {

  Categorie.findAll(
  )
    .then(categorie => {
      if(categorie) {
        res.json(categorie)
      } else {
        res.send('Categorie does not exist')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

categories.post('/delete', (req, res) => {

  Categorie.findOne({
        where: {
            id: req.body.id
        }
    }).then(categorie => {
        if(req.body.id == categorie.id) {

            Categorie.destroy({where: {id : req.body.id}})
            .then(
                res.send('Categorie was removed')
            )
            .catch(err => {
                res.send('Update error: ' + err)
            })

        } else {
            res.send("Categorie does not exist")
        }
    })
    .catch(err => {
        res.send('Find categorie error: ' + err)
    })
})

function setData(req) {
    const today = new Date()
    const categorieData = {
        name: req.body.name,
        created: today
    }

    return categorieData
}

module.exports = categories
