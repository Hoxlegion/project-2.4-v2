/*
    Author: Timo de Jong
    Version: 1.0
    Description: Document containing all the query functions for interactions with the chains table
**/

const express = require("express")
const chains = express.Router()
const cors = require("cors")
const jwt = require("jsonwebtoken")
const bcrypt = require("bcrypt")

const Chain = require("../models/Chain")
chains.use(cors())

process.env.SECRET_KEY = 'secret'

chains.post('/register', (req, res) => {
    let data = setData(req)

    Chain.findOne({
        where: {
            email: req.body.email
        }
    })
        .then(chain => {
            if(!chain) {
                const hash = bcrypt.hashSync(data.password, 10)
                data.password = hash
                Chain.create(data)
                .then(chain => {
                    let token = jwt.sign(chain.dataValues, process.env.SECRET_KEY, {
                        expiresIn: "30d"
                    })
                    res.json({ token:token })
                })
                .catch(err => {
                    res.send('error' + error)
                })
            } else {
                res.json({ error: 'Chain already exists' })
            }
        })
        .catch(err => {
            res.send('error:' + err)
        })
})

chains.post('/login', (req, res) => {
    Chain.findOne({
        where: {
            email: req.body.email
        }
    })
    .then(chain => {
        if(bcrypt.compareSync(req.body.password, chain.password)) {
            let token = jwt.sign(chain.dataValues, process.env.SECRET_KEY, {
                expiresIn: "30d"
            })
            res.json({token:token})
        } else {
            res.send("Chain does not exist")
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

chains.get('/profile', (req, res) => {
    var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

    Chain.findOne({
        where: {
            id: decoded.id
        }
    })
    .then(chain => {
        if(chain) {
            res.json(chain)
        } else {
            res.send('Chain does not exist')
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

chains.post('/update', (req, res) => {
    data = setData(req)

    Chain.findOne({
        where: {
            id: req.body.id
        }
    }).then(chain => {
            Chain.update(data, {where: {id : req.body.id}})
            .then(chain => {
                res.send('chain was updated')
            })
            .catch(err => {
                res.send('Update error: ' + err)
            })
    })
    .catch(err => {
        res.send('Find chain error: ' + err)
    })
})

chains.post('/delete', (req, res) => {
    var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

    Chain.findOne({
        where: {
            id: decoded.id
        }
    }).then(chain => {
        if(decoded.id == chain.id) {
            
            Chain.destroy({where: {id : decoded.id}})
            .then(data => {
                destroyRestaurants(req, res)

                res.send('Chain was removed')
            })
            .catch(err => {
                res.send('Update error: ' + err)
            })

        } else {
            res.send("Chain does not exist")
        }
    })
    .catch(err => {
        res.send('Vind gebruiker error: ' + err)
    })
})

function setData(req) {
    const today = new Date()
    const chainData = {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        streetname: req.body.streetname,
        streetnumber: req.body.streetnumber,
        zipcode: req.body.zipcode,
        city: req.body.city,
        phonenumber: req.body.phonenumber,
        created: today
    }

    return chainData
}

module.exports = chains