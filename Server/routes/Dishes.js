/*
    Author: Timo de Jong
    Version: 1.0
    Description: Document containing all the query functions for interactions with the dishes table
**/

const express = require("express")
const dishes = express.Router()
const cors = require("cors")

const Dish = require("../models/Dish")
const DishIngredient = require("../models/Dish_ingredient")
dishes.use(cors())

dishes.post('/add', (req, res) => {
    let data = setData(req)

    Dish.findOne({
        where: {
            name: req.body.name,
            restaurantId: req.body.restaurantId
        }
    })
    .then(dish => {
        if(!dish) {
            Dish.create(data).then(dishe => {
                for (let ingr of req.body.ingredients.split(',')) {
                    addIngrToDish(dishe, req, ingr)
                }
                res.json({dishe: " has been added"})
            }).catch(err => {
                res.send("Error: " + err)
            })
        } else {
            res.json({ error: 'Dish already exists' })
        }
    })
    .catch(err => {
        res.send('error:' + err)
    })
})

function addIngrToDish(dish, req, ingr) {
    DishIngredient.create({
        dishId: dish.id,
        ingredientId: parseInt(ingr)
    })
    .then(data => {
        console.log("Dish ingredient was added succesfully")
    })
    .catch(err => {
        console.log(err)
    })
}

function removeIngrFromDish(dishId) {
    DishIngredient.destroy({where: {dishId: dishId}})
    .then(
        console.log("Succesfully removed")
    ).catch(err => {
        console.log("Something went wrong: " + err)
    })
}

dishes.post('/getAll', (req, res) => {
    Dish.findAll({
        where: {
            restaurantId: req.body.id
        }
    })
    .then(dish => {
        if(dish) {
            res.json(dish)
        } else {
            res.send('dish does not exist')
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

dishes.post('/getOne', (req, res) => {
    Dish.findOne({
        where: {
            id: req.body.id,
        }
    })
    .then(dish => {
        if(dish) {
            res.json(dish)
        } else {
            res.send('dish does not exist')
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

dishes.post('/update', (req, res) => {
    let data = setData(req)

    Dish.findOne({
        where: {
            id: req.body.id
        }
    }).then(dish => {
        if(req.body.id == dish.id) {

            Dish.update(data, {where: {id : req.body.id}})
            .then(dish => {
                res.send(dish.name + ' was updated')
            })
            .catch(err => {
                res.send('Update error: ' + err)
            })

        } else {
            res.send("Dish does not exist")
        }
    })
    .catch(err => {
        res.send('Find dish error: ' + err)
    })
})

dishes.post('/delete', (req, res) => {
    deleteDish(req, res)
})

function deleteDish(req, res) {
    Dish.findOne({
        where: {
            id: req.body.id,
        }
    }).then(dish => {
        if(dish) {
            Dish.destroy({where: {id : dish.id}})
            .then(data => {
                res.send(req.body.id + ' was removed')
                removeIngrFromDish(dish.id)
            })
            .catch(err => {
                res.send('Update error: ' + err)
            })

        } else {
            res.send("Dish does not exist")
        }
    })
    .catch(err => {
        res.send('Find dish error: ' + err)
    })
}

function setData(req) {
    const today = new Date()
    const dishData = {
        restaurantId: req.body.restaurantId,
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        created: today,
        course: req.body.course,
        allergies: req.body.allergies
    }

    return dishData
}

module.exports = dishes

