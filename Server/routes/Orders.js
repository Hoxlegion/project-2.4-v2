/*
    Author: Timo de Jong
    Version: 1.0
    Description: Document containing all the query functions for interactions with the orders table 
**/

const express = require("express")
const orders = express.Router()
const cors = require("cors")

const Order = require("../models/Order")
orders.use(cors())

orders.post('/add', (req, res) => {
    let data = setData(req)
    Order.create(data)
        .then(
            order => {
                if (order) {
                    res.json(order.id)
                } else {
                    res.send('No orders found')
                }
            }
        )
        .catch(err => {
            res.send('Find orders error: ' + err)
        })
})

orders.get('/getAll', (req, res) => {
    Order.findAll()
        .then(restaurant => {
            if (restaurant) {
                res.json(restaurant)
            } else {
                res.send('No orders found')
            }
        })
        .catch(err => {
            res.send('Find orders error: ' + err)
        })
})

orders.post('/getAllFromRestaurant', (req, res) => {
    Order.findAll({
        where: {
            restaurantId: req.body.restaurantId
        }
    })
        .then(order => {
            if (order) {
                res.json(order)
            } else {
                res.send('No restaurants found')
            }
        })
        .catch(err => {
            res.send('Get orders from restaurant error: ' + err)
        })
})

orders.post('/getAllFromUser', (req, res) => {
    Order.findAll({
        where: {
            userId: req.body.userId
        }
    })
        .then(order => {
            if (order) {
                res.json(order)
            } else {
                res.send('No order found')
            }
        })
        .catch(err => {
            res.send('Get orders from user error: ' + err)
        })
})

orders.post('/getOne', (req, res) => {
    Order.findOne({
        where: {
            id: req.body.id,
        }
    })
        .then(order => {
            if (order) {
                res.json(order)
            } else {
                res.send('Order does not exist')
            }
        })
        .catch(err => {
            res.send('error: ' + err)
        })
})

orders.post('/update', (req, res) => {
    let data = setData(req)

    Order.findOne({
        where: {
            id: req.body.id
        }
    }).then(order => {
        Order.update(data, { where: { id: req.body.id } })
            .then(order => {
                res.send('Review ' + order + ' was updated')
            })
            .catch(err => {
                res.send('Update error: ' + err)
            })
    })
        .catch(err => {
            res.send('Find review error: ' + err)
        })
})

orders.post('/delete', (req, res) => {
    Order.findOne({
        where: {
            id: req.body.id
        }
    }).then(
        res.send("Order has been removed")
    )
        .catch(err => {
            res.send('Error: ' + err)
        })
})

function setData(req) {
    const today = new Date()
    const restaurantData = {
        id: req.body.id,
        restaurantId: req.body.restaurantId,
        dishIds: req.body.dishIds,
        userId: req.body.userId,
        email: req.body.email,
        paymentMethodId: req.body.paymentMethodId,
        price: req.body.price,
        deliveryAddressStreet: req.body.deliveryAddressStreet,
        deliveryAddressNumber: req.body.deliveryAddressNumber,
        deliveryAddressZipcode: req.body.deliveryAddressZipcode,
        deliveryAddressCity: req.body.deliveryAddressCity,
        deliveryTime: req.body.deliveryTime,
        created: today,
        phone: req.body.phone,
        name: req.body.name,
        businessName: req.body.businessName,
        comments: req.body.comments,
        isDelivered: req.body.isDelivered
    }

    return restaurantData
}


module.exports = orders
