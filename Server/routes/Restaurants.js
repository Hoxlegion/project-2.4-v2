/*
    Author: Timo de Jong
    Version: 1.0
    Description: Document containing all the query functions for interactions with the restaurants table
**/

const express = require("express")
const restaurants = express.Router()
const cors = require("cors")

const Restaurant = require("../models/Restaurant")
const Restaurantcatagorie = require("../models/Restaurant_catagorie")

Restaurant.hasMany(Restaurantcatagorie)
restaurants.use(cors())

restaurants.post('/add', (req, res) => {
    let data = setData(req)

    Restaurant.findOne({
        where: {
            name: req.body.name
        }
    })
    .then(restaurant => {
        if(!restaurant) {
            Restaurant.create(data).then(restaurante => {
                for (let categorie of req.body.categories.split(',')) {
                    addCategorieRestaurantLink(restaurante, req, categorie)
                }
            }).catch(err => {
                res.send("error: " + err)
            })

        } else {
            res.json({ error: 'Restaurant already exists' })
        }
    })
    .catch(err => {
        res.send('error:' + err)
    })
})

function addCategorieRestaurantLink(restaurant, req, categorie) {
    Restaurantcatagorie.create({
        categorieId: parseInt(categorie),
        restaurantId: restaurant.id
    })
    .then(data => {
        console.log('Categorie link was made: ' + data)
    })
    .catch(err => {
        console.log('Error linking restaurantCategorie: ' + err)
    })
}

restaurants.get('/getAll', (req, res) => {
    Restaurant.findAll({
      include: [{
        model: Restaurantcatagorie,
        required:true
      }]
      }
    )
    .then(restaurant => {
        if(restaurant) {
            res.json(restaurant)
        } else {
            res.send('No restaurants found')
        }
    })
    .catch(err => {
        res.send('Find restaurants error: ' + err)
    })
})

restaurants.post('/getAllFromChain', (req, res) => {
    Restaurant.findAll({
        where: {
            chainId: req.body.chainId
        }
    })
    .then(restaurant => {
        if(restaurant) {
            res.json(restaurant)
        } else {
            res.send('No restaurants found')
        }
    })
    .catch(err => {
        res.send('Find restaurants error: ' + err)
    })
})

restaurants.post('/getOne', (req, res) => {
    Restaurant.findOne({
        where: {
            id: req.body.id,
        }
    })
    .then(restaurant => {
        if(restaurant) {
            res.json(restaurant)
        } else {
            res.send('Restaurant does not exist')
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

restaurants.post('/update', (req, res) => {
    let data = setData(req)

    Restaurant.findOne({
        where: {
            id: req.body.id,
            chainId: req.body.chainId
        }
    }).then(restaurant => {
        if(req.body.id == restaurant.id) {

            Restaurant.update(data, {where: {id : req.body.id}})
            .then(restaurant => {
                res.send('Restaurant ' + restaurant.name + ' was updated')
            })
            .catch(err => {
                res.send('Update error: ' + err)
            })

        } else {
            res.send("Restaurant does not exist")
        }
    })
    .catch(err => {
        res.send('Find restaurant error: ' + err)
    })
})

restaurants.post('/delete', (req, res) => {

    Restaurant.findOne({
        where: {
            id: req.body.id,
            chainId: req.body.chainId
        }
    }).then(restaurant => {
        destoyRestaurant(req, res)
    })
    .catch(err => {
        res.send('Find restaurant error: ' + err)
    })
})

function destoyRestaurant(req, res) {
    const dishes = require("./Dishes.js")
    if(req.body.id == restaurant.id) {

        Restaurant.destroy({where: {id : erq.body.id}})
        .then(
            res.send('Restaurant was removed')
        )
        .catch(err => {
            res.send('Update error: ' + err)
        })

    } else {
        res.send("Restaurant does not exist")
    }
}

function setCategorieData(req) {
    const restaurantCategorieData = {
        restaurantId: req.body.restaurantId,
        categorieId: req.body.categorieId
    }

    return restaurantCategorieData
}

function setData(req) {
    const today = new Date()
    const restaurantData = {
        id: req.body.id,
        chainId: req.body.chainId,
        name: req.body.name,
        email: req.body.email,
        streetname: req.body.streetname,
        streetnumber: req.body.streetnumber,
        zipcode: req.body.zipcode,
        city: req.body.city,
        phonenumber: req.body.phonenumber,
        minOrderPrice: req.body.minOrderPrice,
        deliveryCosts: req.body.deliveryCosts,
        deliveryZipcodes: req.body.deliveryZipcodes,
        diets: req.body.diets,
        created: today,
        description: req.body.description
    }

    return restaurantData
}


module.exports = restaurants
