/*
    Author: Timo de Jong
    Version: 1.0
    Description: Document containing all the query functions for interactions with the reviews table
**/

const express = require("express")
const reviews = express.Router()
const cors = require("cors")

const Review = require("../models/Review")
reviews.use(cors())

reviews.post('/add', (req, res) => {
    let data = setData(req)
    Review.create(data)
    .then(review => {
        if(review) {
            res.json(review.id)
        } else {
            res.json({ error: 'Review already exists' })
        }
    })
    .catch(err => {
        res.send('error:' + err)
    })
})

reviews.post('/getRestaurant', (req, res) => {
    Review.findAll({
        where: {
            restaurantId: req.body.restaurantId
        }
    })
    .then(review => {
        if(review) {
            res.json(review)
        } else {
            res.send('Review does not exist')
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

reviews.get('/getMy', (req, res) => {
    Review.findOne({
        where: {
            id: req.body.userId
        }
    })
    .then(review => {
        if(review) {
            res.json(review)
        } else {
            res.send('Review does not exist')
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

reviews.post('/getOrderReview', (req, res) => {
    Review.findOne({
        where: {
            orderId: req.body.orderId
        }
    })
    .then(review => {
        if(review) {
            res.json(review)
        } else {
            res.send('Review does not exist')
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

reviews.post('/update', (req, res) => {
    let data = setData(req)

    Review.findOne({
        where: {
            id: req.body.id
        }
    }).then(review => {
        if(req.body.id == review.id) {

            Review.update(data, {where: {id : req.body.id}})
            .then(review => {
                res.send('Review ' + review.name + ' was updated')
            })
            .catch(err => {
                res.send('Update error: ' + err)
            })

        } else {
            res.send("Review does not exist")
        }
    })
    .catch(err => {
        res.send('Find review error: ' + err)
    })
})

reviews.post('/delete', (req, res) => {

    Review.findOne({
        where: {
            id: req.body.id
        }
    }).then(review => {
        if(req.body.id == review.id) {

            Review.destroy({where: {id : erq.body.id}})
            .then(
                res.send('Review was removed')
            )
            .catch(err => {
                res.send('Delete error: ' + err)
            })

        } else {
            res.send("Review does not exist")
        }
    })
    .catch(err => {
        res.send('Find review error: ' + err)
    })

    
})

function setData(req) {
    const today = new Date()
    const reviewData = {
        userId: req.body.userId,
        restaurantId: req.body.restaurantId,
        title: req.body.title,
        description: req.body.description,
        rating: req.body.rating,
        created: today,
        orderId: req.body.orderId
    }
    return reviewData
}

module.exports = reviews