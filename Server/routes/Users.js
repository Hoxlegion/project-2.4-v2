/*
    Author: Timo de Jong
    Version: 1.0
    Description: Document containing all the query functions for interactions with the users table
**/

const express = require("express")
const users = express.Router()
const cors = require("cors")
const jwt = require("jsonwebtoken")
const bcrypt = require("bcrypt")

const User = require("../models/User")
var sql = require("../database/db.js")
users.use(cors())

process.env.SECRET_KEY = 'secret'

users.post('/register', (req, res) => {
    const today = new Date()
    const userData = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        password: req.body.password,
        created: today,
        birth_date: req.body.birth_date
    }

    User.findOne({
        where: {
            email: req.body.email
        }
    })
        .then(user => {
            if (!user) {
                const hash = bcrypt.hashSync(userData.password, 10)
                userData.password = hash
                User.create(userData)
                    .then(user => {
                        let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
                            expiresIn: "30d"
                        })
                        res.json({ token: token })
                    })
                    .catch(err => {
                        res.send('error' + error)
                    })
            } else {
                res.json({ error: 'User already exists' })
            }
        })
        .catch(err => {
            res.send('error:' + err)
        })
})

users.post('/login', (req, res) => {
    User.findOne({
        where: {
            email: req.body.email
        }
    })
        .then(user => {
            if (bcrypt.compareSync(req.body.password, user.password)) {
                let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
                    expiresIn: "30d"
                })
                res.json({ token: token })
            } else {
                res.send("User does not exist")
            }
        })
        .catch(err => {
            res.send('error: ' + err)
        })
})

users.get('/profile', (req, res) => {
    var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

    User.findOne({
        where: {
            id: decoded.id
        }
    })
        .then(user => {
            if (user) {
                res.json(user)
            } else {
                res.send('User does not exist')
            }
        })
        .catch(err => {
            res.send('error: ' + err)
        })
})

users.post("/profile", (req, res) => {
    var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

    sql.query("UPDATE users SET last_name='S-A' WHERE id=" + decoded.id)
    if (err) {
        console.log("error: ", err);
        result(err, null);
    }
    else {
        result(null, res.insertId);
    }
})

users.get('/allUsers', (req, res) => {
    User.findAll()
        .then(user => {
            if (user) {
                res.json(user)
            } else {
                res.send('User does not exist')
            }
        })
        .catch(err => {
            res.send('error: ' + err)
        })
})

users.post('/update', (req, res) => {
    var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)
    let data = setData(req);

    User.findOne({
        where: {
            id: decoded.id
        }
    }).then(user => {
        if (decoded.id == user.id) {

            User.update(data, { where: { id: decoded.id } })
                .then(user => {
                    res.send('User was updated')
                })
                .catch(err => {
                    res.send('Update error: ' + err)
                })

        } else {
            res.send("User does not exist")
        }
    })
        .catch(err => {
            res.send('Find user error: ' + err)
        })
})

users.post("/updatePassword", (req, res) => {
    var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)
    let data = setData(req);

    User.findOne({
        where: {
            id: decoded.id
        }
    }).then(user => {
        if (bcrypt.compareSync(req.body.currentPassword, user.password)) {
            data.password = bcrypt.hashSync(req.body.password, 10)
            User.update(data, { where: { id: decoded.id } })
                .then(user => {
                    res.send('User was updated')
                })
                .catch(err => {
                    res.send('Update error: ' + err)
                })

        } else {
            res.send("Current password does not match")
        }
    })
        .catch(err => {
            res.send('Find user error: ' + err)
        })

})

users.post('/delete', (req, res) => {
    var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

    User.findOne({
        where: {
            id: decoded.id
        }
    }).then(user => {
        if (decoded.id == user.id) {

            User.destroy({ where: { id: decoded.id } })
                .then(
                    res.send('User was removed')
                )
                .catch(err => {
                    res.send('Update error: ' + err)
                })

        } else {
            res.send("User does not exist")
        }
    })
        .catch(err => {
            res.send('Vind gebruiker error: ' + err)
        })
})

function setData(req) {
    const today = new Date()
    const userData = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        password: req.body.password,
        created: today,
        birth_date: req.body.birth_date
    }

    return userData
}

module.exports = users
