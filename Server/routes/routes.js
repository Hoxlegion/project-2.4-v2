/*
    Author: Timo de Jong
    Version: 1.0
    Description: Document fetching all the incomming requests and routing them to the fitting document
**/

const express = require('express');
const router = express.Router();

const users = require('./users');
const chains = require('./chains');
const restaurants = require('./restaurants');
const dishes = require('./dishes');
const reviews = require('./reviews');
const categories = require('./categories');
const orders = require('./orders');

router.get('/', (req, res, next) => {
  res.render('It works');
})

router.get('/chains', (req, res, next) => {
  res.json({item: 'Welcome'});
})
.use('/chains', chains);

router.get('/users', (req, res, next) => {
  res.json({item: 'Welcome'});
})
.use('/users', users);

router.get('/restaurants', (req, res, next) => {
  res.json({item: 'Welcome'});
})
.use('/restaurants', restaurants);

router.get('/dishes', (req, res, next) => {
  res.json({item: 'Welcome'});
})
.use('/dishes', dishes);

router.get('/reviews', (req, res, next) => {
  res.json({item: 'Welcome'});
})
.use('/reviews', reviews);

router.get('/categories', (req, res, next) => {
  res.json({item: 'Welcome'});
})
.use('/categories', categories);

router.get('/orders', (req, res, next) => {
  res.json({item: 'Welcome'});
})
.use('/orders', orders);

module.exports = router
