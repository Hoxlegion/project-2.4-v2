import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserSearchComponent } from './user-search/user-search.component';
import { UserViewComponent } from './user-view/user-view.component';



@NgModule({
  declarations: [UserSearchComponent, UserViewComponent],
  imports: [
    CommonModule
  ]
})
export class AdminModule { }
