import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeModule } from './home/home.module';
import { LoggerModule } from './logger/logger.module';
import { UserInfoModule } from './user-info/user-info.module';
import { ChainModule } from './chain/chain.module';
import { AuthGuardService as guard, AuthGuardService } from './auth/users/auth-guard.service';
import { AuthService } from './auth/users/auth.service';
import { FilterComponent} from './restaurants/filter/filter.component';
import { CheckOutModule } from './check-out/check-out.module';
import { ChainAuthService } from './auth/chains/chain-auth.service';
import { RestaurantReviewsModule } from "./restaurant-reviews/restaurant-reviews.module";
import { RestaurantMenuModule } from "./restaurant-menu/restaurant-menu.module";
import { ShoppingCartModule } from './shopping-cart/shopping-cart.module';
import { MyRestaurantModule } from './my-restaurant/my-restaurant.module';
import { ThisRestaurantModule } from './this-restaurant/this-restaurant.module';
import { OrderOverviewModule } from './order-overview/order-overview.module';
import {WildcardComponent} from "./wildcard/wildcard/wildcard.component"

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomeModule' },
  { path: 'gebruiker', loadChildren: './logger/logger.module#LoggerModule' },
  { path: 'chain', loadChildren: './chain/chain.module#ChainModule' },
  { path: 'profiel', loadChildren: './user-info/user-info.module#UserInfoModule', canActivate: [AuthGuardService] },
  { path: 'restaurants/:code', loadChildren: './restaurants/restaurants.module#RestaurantsModule', component: FilterComponent },
  { path: "reviews", loadChildren: "./restaurant-reviews/restaurant-reviews.module#RestaurantReviewsModule" },
  { path: "check-out", loadChildren: "./check-out/check-out.module#CheckOutModule" },
  { path: "menu", loadChildren: "./restaurant-menu/restaurant-menu.module#RestaurantMenuModule" },
  { path: "shopping-cart", loadChildren: "./shopping-cart/shopping-cart.module#ShoppingCartModule" },
  { path: "mijnrestaurants", loadChildren: "./my-restaurant/my-restaurant.module#MyRestaurantModule" },
  { path: "dit-restaurant", loadChildren: "./this-restaurant/this-restaurant.module#ThisRestaurantModule" },
  { path: "order-overview", loadChildren: "./order-overview/order-overview.module#OrderOverviewModule" },
  { path: "**", component: WildcardComponent }
];

@NgModule({
  imports: [
    HomeModule,
    LoggerModule,
    UserInfoModule,
    ChainModule,
    RouterModule,
    ShoppingCartModule,
    RestaurantReviewsModule,
    CheckOutModule,
    RestaurantMenuModule,
    MyRestaurantModule,
    ThisRestaurantModule,
    RouterModule.forRoot(routes, { useHash: true }),
    OrderOverviewModule
  ],
  providers: [guard, AuthService, ChainAuthService],
  exports: [RouterModule]
})
export class AppRoutingModule { }
