/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { ChainAuthService } from './chain-auth.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(public auth: ChainAuthService, public router: Router) {}

  canActivate(): boolean {
    if (!this.auth.isLoggedIn()) {
      this.router.navigate(['chain/login']);
      return false;
    }
    return true;
  }

}