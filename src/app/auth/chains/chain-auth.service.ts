/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { Router } from '@angular/router'

export interface ChainDetails {
    id: number
    name: string
    email: string
    password: string
    streetname: string
    streetnumber: string
    zipcode: string
    city: string
    phonenumber: number
    exp: number
    iat: number
}

export interface ChainDetailsUpdate {
    id: number
    name: string
    email: string
    password: string
    streetname: string
    streetnumber: string
    zipcode: string
    city: string
    phonenumber: number
}

export interface ChainTokenPayload {
    id: number
    name: string
    email: string
    password: string
    streetname: string
    streetnumber: string
    zipcode: string
    city: string
    phonenumber: number
}

interface TokenReponse {
    token: string
}

@Injectable()
export class ChainAuthService {
    private token: string
    
    constructor(private http: HttpClient, private router: Router) {}

    private setSession(token: string) : void {
        console.log("Setting session")
 
        localStorage.setItem('chainToken', token)
        this.token = token
    }

    private getToken (): string {
        if(!this.token) {
            this.token = localStorage.getItem('chainToken')
        }
        return this.token
    }

    public getChainDetails(): ChainDetails {
        const token = this.getToken()
        let payload
        if(token) {
            payload = token.split('.')[1]
            payload = window.atob(payload)
            return JSON.parse(payload)
        } else {
            return null
        }
    }

    public isLoggedIn() : boolean {
        const chain = this.getChainDetails()
        
        if(chain) {
            return chain.exp > Date.now() / 1000
        } else {
            return false
        }
    }

    public chainRegister(chain: ChainTokenPayload) : Observable<any> {
        const base = this.http.post(`/chains/register`, chain)

        const request = base.pipe(
            map((data: TokenReponse) => {
                if(data.token) {
                    this.setSession(data.token)
                }
                return data
            })
        )

        return request
    }

    public chainLogin(chain: ChainTokenPayload) : Observable<any> {
        const base = this.http.post(`/chains/login`, chain)

        const request = base.pipe(
            map((data: TokenReponse) => {
                if(data.token) {
                    this.setSession(data.token)
                }
                return data
            })
        )

        return request
    }

    public chainProfile() : Observable<any> {
        return this.http.get('/chains/profile' , {
            headers: { Authorization: `${this.getToken()}` }
        }) 
    }

    public updateProfile(chain: ChainDetailsUpdate) : Observable<any>{
        const base = this.http.post("/chains/update", chain, {responseType: "text"})
        return base;
    }

    public chainLogout(): void {
        this.token = ''
        window.localStorage.removeItem('chainToken')
        this.router.navigate(['/'])
    }
}