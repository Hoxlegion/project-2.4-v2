/*
    Author: Joris Westera
    Version: 1.0
    Description: Services used for check-out
**/

import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { Router } from '@angular/router'

export interface checkOutDetails {
  id: number
  restaurantId: number
  dishIds: string
  userId: number
  email: string
  paymentMethodId: number
  price: string
  deliveryAddressStreet: string
  deliveryAddressNumber: number
  deliveryAddressZipcode: string
  deliveryAddressCity: string
  deliveryTime: string
  created: number
  phone: number
  name: string
  businessName: string
  comments: string
  isDelivered: number
}

export interface ReviewDetails {
  userId: number
  restaurantId: number
  title: string
  description: string
  rating: number
  orderId: string
}

export interface updateDelivered {
  id: 0,
  isDelivered: number
}

@Injectable()
export class CheckOutService {
  private token: string

  constructor(private http: HttpClient, private router: Router) { }

  public checkOut(order: checkOutDetails):Observable<any>{
    return this.http.post('/orders/add', order)
  }

  public getOrder(id): Observable<any>{
    return this.http.post("orders/getOne", {id:id})
  }

  public getReview(orderId): Observable<any>{
    return this.http.post("reviews/getOrderReview", {orderId:orderId})
  }

  public setReview(review: ReviewDetails): Observable<any>{
    return this.http.post("reviews/add", review)
  }

  public getRestaurantOrders(restaurantId): Observable<any> {
    return this.http.post("orders/getAllFromRestaurant", {restaurantId: restaurantId})
  }

  public getUserOrders(userId): Observable<any> {
    return this.http.post("orders/getAllFromUser", {userId: userId})
  }

  public updateOrderDelivered(isDelivered: updateDelivered) : Observable<any> {
    const base = this.http.post("/orders/update", isDelivered, {responseType: "text"})
    return base
  }
}
