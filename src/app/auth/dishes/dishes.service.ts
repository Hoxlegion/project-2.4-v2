/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

export interface menuDetails {
  name: string,
  description: string,
  price: number,
  allergies: string
}

export interface menuAddDetails {
  id: number,
  restaurantId: number,
  name: string,
  description: string,
  ingredients: string,
  price: number,
  allergies: string,
  course: string
}

export interface restaurantId{
  id: number;
}

@Injectable({
  providedIn: 'root'
})

export class DishesService {

  constructor(private http: HttpClient, private router: Router) { }

  public getMenu(id: restaurantId): Observable<any> {
    return this.http.post("/dishes/getAll", {id:id});
  }

  public getDish(id): Observable<any> {
    return this.http.post("/dishes/getOne", {id:id})
  }

  public addDish(dish: menuAddDetails): Observable<any> {
    const base = this.http.post("/dishes/add", dish)
    return base
  }

  public updateDish(dish: menuAddDetails) : Observable<any> {
    const base = this.http.post("/dishes/update", dish, {responseType: "text"})
    return base
  }

  public deleteDish(dishId){
    const base = this.http.post("dishes/delete", {id:dishId});
    return base;
  }
}
