import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

export interface oneRestaurant {
  id: number,
  chainId: number,
  name: string,
  email: string,
  streetname: string,
  streetnumber: string,
  zipcode: string,
  city: string,
  phonenumber: string;
  minOrderPrice: number,
  deliveryCosts: number,
  deliveryZipcodes: string,
  categories: string,
  description: string
}

export interface oneRestaurantUpdate {
  name: string,
  email: string,
  streetname: string,
  streetnumber: string,
  zipcode: string,
  city: string,
  phonenumber: string;
  minOrderPrice: number,
  deliveryCosts: number,
  deliveryZipcodes: string,
  categories: string,
  description: string
}

export interface restaurantId{
  id: number;
}

@Injectable({
  providedIn: 'root'
})

export class RestaurantServiceService {

  constructor(private http: HttpClient, private router: Router) { }

  public getRestaurant(id): Observable<any> {
    const base = this.http.post("/restaurants/getOne", {id:id})
    return base;
  }

  public allRestaurants(): Observable<any> {
    return this.http.get('/restaurants/getAll');
  }

  public allCategories(): Observable<any> {
    return this.http.get('/categories/getAll');
  }

  public allFromChain(chainId: number): Observable<any> {
    const res = this.http.post("/restaurants/getAllFromChain", {chainId: chainId})
    return res;
  }

  public newRestaurant(restaurant: oneRestaurant) : Observable<any> {
    const base = this.http.post(`/restaurants/add`, restaurant)
    return base
  }

  public updateRestaurant(restaurant: oneRestaurantUpdate) : Observable<any> {
    const base = this.http.post("/restaurants/update", restaurant, {responseType: "text"})
    return base
  }

}


