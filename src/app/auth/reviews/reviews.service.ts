/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: A service which is used 
*/

import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { Router } from '@angular/router'

export interface reviews {
    userId: number,
    restaurantId: number,
    title: string,
    description: string,
    rating: number
}

@Injectable({
    providedIn: 'root'
})

export class ReviewAuthService {
    private token: string;

    constructor(private http: HttpClient, private router: Router) { }

    /**
     * @param restaurantId  The id of a restaurant
     * @return base         An object which contains all the data found in the database.
     */
    public getReviews(restaurantId:number): Observable<any> {
        const base = this.http.post("/reviews/getRestaurant", {restaurantId:restaurantId});
        return base;
    }
}