/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { Router } from '@angular/router'

export interface UserDetails {
    id: number
    first_name: string
    last_name: string
    email: string
    password: string
    birth_date: Date
    exp: number
    iat: number
}

export interface UserDetailsUpdate {
    first_name: string,
    last_name: string,
    email: string,
    birth_date: Date
}

export interface passwordUpdate {
    password: string,
    currentPassword: string
}

export interface TokenPayload {
    id: number
    first_name: string
    last_name: string
    email: string
    password: string
    birth_date: Date
}

interface TokenReponse {
    token: string
}


@Injectable()
export class AuthService {
    private token: string

    constructor(private http: HttpClient, private router: Router) { }

    private setSession(token: string): void {
        localStorage.setItem('userToken', token)
        this.token = token
    }

    private getToken(): string {
        if (!this.token) {
            this.token = localStorage.getItem('userToken')
        }
        return this.token
    }

    public getUserDetails(): UserDetails {
        const token = this.getToken()
        let payload
        if (token) {
            payload = token.split('.')[1]
            payload = window.atob(payload)
            return JSON.parse(payload)
        } else {
            return null
        }
    }

    public isLoggedIn(): boolean {
        const user = this.getUserDetails()

        if (user) {
            return user.exp > Date.now() / 1000
        } else {
            return false
        }
    }

    public register(user: TokenPayload): Observable<any> {
        const base = this.http.post('/users/register', user)

        const request = base.pipe(
            map((data: TokenReponse) => {
                if (data.token) {
                    this.setSession(data.token)
                }
                return data
            })
        )

        return request
    }

    public login(user: TokenPayload): Observable<any> {
        const base = this.http.post(`/users/login`, user);

        const request = base.pipe(
            map((data: TokenReponse) => {
                if (data.token) {
                    this.setSession(data.token);
                }
                return data;
            })
        );

        return request;
    }

    public profile(): Observable<any> {
        return this.http.get('/users/profile', {
            headers: { Authorization: `${this.getToken()}` }
        })
    }

    public updateProfile(user: UserDetailsUpdate): Observable<any> {
        const base = this.http.post("/users/update", user,  {headers: {Authorization: `${this.getToken()}`}})
        return base;
    }

    public updatePassword(password: passwordUpdate): Observable<any>{
        const base = this.http.post("users/updatePassword", password, {headers: {Authorization: `${this.getToken()}`}});
        return base;
    }

    public logout(): void {
      this.token = ''
      window.localStorage.removeItem('userToken')
      window.localStorage.removeItem('chainToken')
      this.router.navigate(['/']).then(r => window.location.reload())
    }
}
