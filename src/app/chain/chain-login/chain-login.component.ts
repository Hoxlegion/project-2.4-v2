/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ChainAuthService, ChainTokenPayload } from '../../auth/chains/chain-auth.service';

@Component({
  selector: 'app-chain-login',
  providers: [ChainAuthService],
  templateUrl: './chain-login.component.html',
  styleUrls: ['./chain-login.component.css']
})
export class ChainLoginComponent {

  check = false;

  credentials: ChainTokenPayload = {
    id: 0,
    name: '',
    email: '',
    password: '',
    streetname: '',
    streetnumber: '',
    city: '',
    zipcode: '',
    phonenumber: 0
  }

  form = new FormGroup({
    email: new FormControl(),
    password: new FormControl()
  })

  constructor(
    private fb:FormBuilder,
    private chainAuthService: ChainAuthService,
    private router: Router
  ) {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  login() {
    this.chainAuthService.chainLogin(this.credentials).subscribe(
      () => {
        this.router.navigate(['/profiel-chain'])
      },
      err => {
        console.error(err)
        this.check = true
      }
    )
  }

}
