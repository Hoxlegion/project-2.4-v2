/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chain-profile',
  templateUrl: './chain-profile.component.html',
  styleUrls: ['./chain-profile.component.css']
})
export class ChainProfileComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  toMyData() {
    this.router.navigate(['gegevens']);
  }

  toMyRestaurants() {
    this.router.navigate(['mijnrestaurants/lijst']);
  }

}
