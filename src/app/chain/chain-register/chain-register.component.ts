/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ChainAuthService, ChainTokenPayload } from '../../auth/chains/chain-auth.service';

@Component({
  selector: 'app-chain-register',
  providers: [ChainAuthService],
  templateUrl: './chain-register.component.html',
  styleUrls: ['./chain-register.component.css']
})
export class ChainRegisterComponent {
  credentials: ChainTokenPayload = {
    id: 0,
    name: '',
    email: '',
    password: '',
    streetname: '',
    streetnumber: '',
    zipcode: '',
    city: '',
    phonenumber: 0
  }

  form = new FormGroup ({
    name: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
    passwordRe: new FormControl(),
    streetname: new FormControl(),
    streetnumber: new FormControl(),
    zipcode: new FormControl(),
    city: new FormControl(),
    phonenumber: new FormControl()
  })

  constructor(
    private fb:FormBuilder,
    private chainAuthService: ChainAuthService,
    private router: Router
  ) { 
    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      passwordRe: ['', Validators.required],
      streetname: ['', Validators.required],
      streetnumber: ['', Validators.required],
      zipcode: ['', Validators.required],
      city: ['', Validators.required],
      phonenumber: ['', Validators.required]
    })
  }

  chainRegister() {
    this.chainAuthService.chainRegister(this.credentials).subscribe(
      () => {
        this.router.navigate(['/login'])
      },
      err => {
        console.error(err)
      }
    )
  }


}
