import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { Routes, RouterModule } from '@angular/router';
import { ChainComponent } from './chain.component';
import { ChainLoginComponent } from './chain-login/chain-login.component';
import { ChainRegisterComponent } from './chain-register/chain-register.component';
import { ChainProfileComponent } from './chain-profile/chain-profile.component';
import { MyDataComponent } from './my-data/my-data.component';
import { NewRestaurantComponent } from './new-restaurant/new-restaurant.component';

const chainRoutes:Routes = [
  { path: '', component: ChainComponent, children: [
    { path: 'login', component: ChainLoginComponent},
    { path: 'registreren', component: ChainRegisterComponent},
    { path: 'profiel-chain', component: ChainProfileComponent },
    { path: 'gegevens', component: MyDataComponent },
    { path: 'nieuwRestaurant', component: NewRestaurantComponent }
  ]}
]

@NgModule({
  declarations: [
    ChainLoginComponent,
    ChainRegisterComponent,
    ChainComponent,
    ChainProfileComponent,
    MyDataComponent,
    NewRestaurantComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(chainRoutes)
  ]
})
export class ChainModule { }
