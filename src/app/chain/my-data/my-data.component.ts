/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ChainAuthService, ChainDetails, ChainDetailsUpdate } from '../../auth/chains/chain-auth.service';

@Component({
  selector: 'app-my-data',
  templateUrl: './my-data.component.html',
  styleUrls: ['./my-data.component.css']
})
export class MyDataComponent implements OnInit {
  details: ChainDetails
  editingMode = true

  detailsToUpdate: ChainDetailsUpdate = {
    id: this.chainAuthService.getChainDetails().id,
    name: "",
    email: "",
    password: this.chainAuthService.getChainDetails().password,
    streetname: "",
    streetnumber: "",
    zipcode: "",
    city: "",
    phonenumber: 0
  }

  form = new FormGroup ({
    name: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
    passwordRe: new FormControl(),
    streetname: new FormControl(),
    streetnumber: new FormControl(),
    zipcode: new FormControl(),
    city: new FormControl(),
    phonenumber: new FormControl()
  })

  constructor( private fb:FormBuilder, private chainAuthService: ChainAuthService, private router: Router ) {

    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      passwordRe: ['', Validators.required],
      streetname: ['', Validators.required],
      streetnumber: ['', Validators.required],
      zipcode: ['', Validators.required],
      city: ['', Validators.required],
      phonenumber: ['', Validators.required]
    })
  }

  ngOnInit(): void {
    this.chainAuthService.chainProfile().subscribe(
      chain => {
        this.details = chain;
        this.setChainDetails(this.details);
      },
      err => {
        console.error(err)
      }
    )

  }

  setChainDetails(details){
    this.form.patchValue({
      name: details.name,
      email: details.email,
      password: details.password,
      streetname: details.streetname,
      streetnumber: details.streetnumber,
      zipcode: details.zipcode,
      city: details.city,
      phonenumber: details.phonenumber
    });
  }

  enableEditing() {
    this.editingMode = !this.editingMode;
  }

  updateChainInfo() {
    this.chainAuthService.updateProfile(this.detailsToUpdate).subscribe(
      () => {
        this.chainAuthService.chainLogout();
        //this.editingMode = !this.editingMode
      },
      err => {
        console.log(err)
      }
    )
  }
}
