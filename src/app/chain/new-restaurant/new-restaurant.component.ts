import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { oneRestaurant, RestaurantServiceService } from '../../auth/restaurantService/restaurant-service.service'
import { Router } from '@angular/router';
import { ChainAuthService } from '../../auth/chains/chain-auth.service';


@Component({
  selector: 'app-new-restaurant',
  templateUrl: './new-restaurant.component.html',
  styleUrls: ['./new-restaurant.component.css']
})
export class NewRestaurantComponent implements OnInit {
  chain = this.chainService.getChainDetails()

  credentials: oneRestaurant = {
    id: 0,
    chainId: this.chain.id,
    name: '',
    email: '',
    streetname: '',
    streetnumber: '',
    zipcode: '',
    city: '',
    phonenumber: '',
    minOrderPrice: 0,
    deliveryCosts: 0,
    deliveryZipcodes: '',
    categories: '',
    description: ''
  }

  form = new FormGroup ({
    name: new FormControl(),
    email: new FormControl(),
    streetname: new FormControl(),
    streetnumber: new FormControl(),
    zipcode: new FormControl(),
    city: new FormControl(),
    phonenumber: new FormControl(),
    minOrderPrice: new FormControl(),
    deliveryCosts: new FormControl(),
    deliveryZipcodes: new FormControl(),
    categories: new FormControl(),
    description: new FormControl()
  })


  constructor(public resService: RestaurantServiceService, public router: Router, private chainService: ChainAuthService, private fb:FormBuilder) {
    this.form = this.fb.group(
      {
        name: ['', Validators.required],
        email: ['', Validators.required],
        streetname: ['', Validators.required],
        streetnumber: ['', Validators.required],
        zipcode: ['', Validators.required],
        city: ['', Validators.required],
        phonenumber: ['', Validators.required],
        minOrderPrice: ['', Validators.required],
        deliveryCosts: ['', Validators.required],
        deliveryZipcodes: ['', Validators.required],
        categories: [''],
        description:['', Validators.required]
      }
    )

  }

  ngOnInit(): void {
  }

  addRestaurant() {
    this.resService.newRestaurant(this.credentials).subscribe(
      () => {
        this.router.navigate(['/mijnrestaurants/lijst'])
      },
      err => {
        console.error(err)
      }
    )
  }

}
