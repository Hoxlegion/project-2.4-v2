/*
    Author: Joris Westera
    Version: 1.0
    Description: Containter for the checkout.
**/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService, TokenPayload } from '../../auth/users/auth.service';
import { RestaurantServiceService, oneRestaurant } from '../../auth/restaurantService/restaurant-service.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CheckOutService, checkOutDetails} from '../../auth/check-out/check-out.service';


@Component({
  selector: 'app-check-out-container',
  templateUrl: './check-out-container.component.html',
  styleUrls: ['./check-out-container.component.css'],
  providers: [CheckOutService]
})
export class CheckOutContainerComponent implements OnInit{
  order: checkOutDetails = {
    id: 0,
    restaurantId: 0,
    dishIds: '',
    userId: 0,
    email: '',
    paymentMethodId: 0,
    price: '',
    deliveryAddressStreet: '',
    deliveryAddressNumber: 0,
    deliveryAddressZipcode: '',
    deliveryAddressCity: '',
    deliveryTime: '',
    created: 0,
    phone: 0,
    name: '',
    businessName: '',
    comments: '',
    isDelivered: 0
  }

  token: TokenPayload

  checkOutForm = new FormGroup({
    adres: new FormControl(),
    adresNumber: new FormControl(),
    city: new FormControl(),
    zipcode: new FormControl(),
    name: new FormControl(),
    email: new FormControl(),
    phone: new FormControl(),
    businessName: new FormControl(),
    deliveryName: new FormControl(),
    payment: new FormControl(),
    comments: new FormControl(),
  })

  public price = 0;
  restaurant: oneRestaurant
  id: number;
  selectData = []

  constructor(
    private fb:FormBuilder,
    private AuthService: AuthService,
    private checkOutService: CheckOutService,
    private router: Router,
    private route: ActivatedRoute,
    private restaurantService: RestaurantServiceService
    ) {
      this.checkOutForm = this.fb.group({
        adres: ['', Validators.required],
        adresNumber: ['', Validators.required],
        city: ['', Validators.required],
        zipcode: ['', Validators.required],
        name: ['', Validators.required],
        email: ['', Validators.required],
        phone: ['', Validators.required],
        businessName: [''],
        deliveryTime: ['', Validators.required],
        payment: ['',Validators.required],
        comments: [''],
      })
    }

    ngOnInit(): void{
      this.token = this.AuthService.getUserDetails()
      if(this.token != null){
        this.order.userId = this.token.id
      }
      this.order.paymentMethodId = 1
      this.order.deliveryTime = "Zo snel mogelijk"
      this.route.params.subscribe(params =>{
        this.id = params["id"];
      });
      this.restaurantService.getRestaurant(this.id).subscribe(
        restaurant => {
          this.restaurant = restaurant
          this.order.restaurantId = this.restaurant.id
          this.order.price = JSON.parse(localStorage.getItem("cart"))
          this.order.dishIds = localStorage.getItem("Order")
          this.price = parseInt(this.order.price)
          this.order.price += this.restaurant.deliveryCosts
         },
         err => {
           console.error(err);
         }
      )

      let hours = new Date().getHours()
      let minutes = new Date().getMinutes()

      // kan aangepast worden naar minimale bezorgtijd restaurant (bestaat nog niet)
      minutes += 30
      minutes = Math.ceil(minutes/15)*15;
      let m
      let h
      for(let i = 0; i < 6; i++){
        if(minutes >= 59){
          minutes -= 60
          hours += 1
          if(hours >= 23){
            hours -= 24
          }
        }
        if(minutes < 10){ m = "0" + minutes}else{m = minutes}
        if(hours < 10){h = "0" + hours}else{h = hours}
        this.selectData.push(h + ":" + m)
        minutes += 15
      }
    }

    checkOut() {
      //temp
      this.order.isDelivered=0

      this.checkOutService.checkOut(this.order).subscribe(
        id => {
          this.id=id
          this.router.navigate(['order-overview/'+ this.id])
        })
    }
}
