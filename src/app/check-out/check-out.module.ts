/*
    Author: Joris Westera
    Version: 1.0
    Description: Model for the check-out.
**/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from '@angular/router';

import { CheckOutComponent } from './check-out.component';
import { CheckOutContainerComponent } from './check-out-container/check-out-container.component';

const loggerRoutes:Routes = [
  { path: '', component:CheckOutComponent, children: [
    { path: ':id', component: CheckOutContainerComponent }
  ]}
]

@NgModule({
  declarations: [
    CheckOutContainerComponent, 
    CheckOutComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(loggerRoutes)
  ]
})
export class CheckOutModule { }
