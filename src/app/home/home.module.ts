import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from './homepage/homepage.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from '@angular/router';

const homeRoutes:Routes = [
  { path: '', component: HomepageComponent }
]

@NgModule({
  declarations: [
    HomepageComponent,
  ],
  exports: [],
  providers: [],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(homeRoutes)
  ]
})

export class HomeModule { }
