/*
    Author: Victor Vrancianu
    Version: 1.0
    Description:
**/


import { Component, OnInit } from '@angular/core';
import { Routes, Router } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {


  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  buttonclick(){
  }

  toRestaurants(code: String){
    code = code[0] + code[1];
    this.router.navigate(['restaurants', code])
  }

}
