/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logger',
  templateUrl: './logger.component.html'
})
export class LoggerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
}
