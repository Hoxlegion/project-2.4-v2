import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from '@angular/router';

import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LoggerComponent } from './logger.component';


const loggerRoutes:Routes = [
  { path: '', component:LoggerComponent, children: [
    { path: 'login', component: LoginComponent },
    { path: 'registreren', component: RegisterComponent },
    { path: 'wachtwoordReset', component: ForgotPasswordComponent }
  ]}
]

@NgModule({
  declarations: [
    LoginComponent, 
    RegisterComponent, 
    ForgotPasswordComponent,
    LoggerComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(loggerRoutes)
  ]
})
export class LoggerModule { }
