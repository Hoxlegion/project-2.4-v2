/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService, TokenPayload } from '../../auth/users/auth.service';

@Component({
  selector: 'app-login',
  providers: [AuthService],
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent {

  check = false;

    credentials: TokenPayload = {
      id: 0,
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      birth_date: new Date
    }

    form = new FormGroup({
      email: new FormControl(),
      password: new FormControl()
    })

    constructor(
      private fb:FormBuilder,
      private authService: AuthService,
      private router: Router
    ) {
      this.form = this.fb.group({
        email: ['', Validators.required],
        password: ['', Validators.required]
      })
    }

    login() {
      this.authService.login(this.credentials).subscribe(
        () => {
          this.router.navigate(['profiel/p'])
        },
        err => {
          console.error(err)
          this.check = true
        }
      )
    }

    toPasswordReset() {
      this.router.navigate(['wachtwoordReset']);
    }
}
