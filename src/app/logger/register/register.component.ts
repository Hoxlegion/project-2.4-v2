/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService, TokenPayload } from '../../auth/users/auth.service';

@Component({
  selector: 'app-register',
  providers: [AuthService],
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  credentials: TokenPayload = {
    id: 0,
    first_name: '',
    last_name: '',
    email: '',
    password: '',
    birth_date: new Date
  }

  form = new FormGroup({
    first_name: new FormControl(),
    last_name: new FormControl(),
    email: new FormControl(),
    birth_date: new FormControl(),
    password: new FormControl(),
    passwordRe: new FormControl()
  })

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.form = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.required],
      birth_date: ["", Validators.required],
      password: ['', Validators.required],
      passwordRe: ['', Validators.required]
    })
  }

  register() {
    if (this.form.controls["first_name"].valid && this.form.controls["last_name"].valid && this.form.controls["email"].valid && this.form.controls["birth_date"].valid && this.form.controls["password"].valid && this.form.controls["passwordRe"].valid) {
      this.authService.register(this.credentials).subscribe(
        () => {
          this.router.navigate(['profiel/p'])
        },
        err => {
          console.error(err)
        }
      )
    }
    else{
      alert("")
    }
  }
}
