import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { RestaurantServiceService } from '../../auth/restaurantService/restaurant-service.service';
import { ChainAuthService } from '../../auth/chains/chain-auth.service';

@Component({
  selector: 'app-my-restaurant-container',
  templateUrl: './my-restaurant-container.component.html',
  styleUrls: ['./my-restaurant-container.component.css']
})
export class MyRestaurantContainerComponent implements OnInit {
  restaurantDataHolder: any;

  constructor(private route: ActivatedRoute, private router: Router, private restaurantService: RestaurantServiceService, private chainService: ChainAuthService) { }

  ngOnInit(): void {
    const chain = this.chainService.getChainDetails()

    this.restaurantService.allFromChain(chain.id).subscribe(
      restaurants => {
        this.restaurantDataHolder = restaurants
      }
    )
  }

  newRestaurant() {
    this.router.navigate(["/chain/nieuwRestaurant"])
  }

  goToRestaurant(id:number) {
    this.router.navigate(["/dit-restaurant/", id])
  }

}
