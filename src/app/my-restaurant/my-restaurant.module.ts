import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MyRestaurantContainerComponent } from './my-restaurant-container/my-restaurant-container.component';

const loggerRoutes:Routes = [
  { path: '', component:MyRestaurantContainerComponent, children: [
    { path: 'lijst', component: MyRestaurantContainerComponent }
  ]}
]


@NgModule({
  declarations: [MyRestaurantContainerComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(loggerRoutes)
  ]
})
export class MyRestaurantModule { }
