/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/users/auth.service';
import { ChainAuthService } from 'src/app/auth/chains/chain-auth.service';

@Component({
  selector: 'app-navbar',
  template: `
    <nav class="navbar navbar-light navbar-expand-md bg-faded justify-content-center">
      <a href="/" class="navbar-brand d-flex w-50 mr-auto">Het is Etenstijd</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar3">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse collapse w-100" id="collapsingNavbar3">
          <div class="navbar-nav w-100 justify-content-center" *ngIf="!chainAuth.isLoggedIn()">
              <form class="form-inline">
                  <input class="form-control mr-sm-2" #code type="text" placeholder="Adres">
                <button (click)="toRestaurants(code.value)" type="submit" class="btn btn-primary">Zoek</button>
              </form>
          </div>

          <ul class="nav navbar-nav ml-auto w-100 justify-content-end">

          <!--<li class='nav-item' *ngIf="!auth.isLoggedIn() && !chainAuth.isLoggedIn()">
              <a class="nav-link" (click)='toCheckOut()'>Afrekenen</a>
          </li>-->

            <li class='nav-item' *ngIf="!auth.isLoggedIn() && !chainAuth.isLoggedIn()">
                <a class="nav-link" (click)='toLogin()'>Login</a>
            </li>

            <li class='nav-item' *ngIf="!auth.isLoggedIn() && !chainAuth.isLoggedIn()">
                <a class="nav-link" (click)='toRegister()'>Registreren</a>
            </li>

            <li class='nav-item' *ngIf="chainAuth.isLoggedIn()">
                <a class="nav-link" (click)='toChainProfile()'>Profiel</a>
            </li>

            <li class='nav-item' *ngIf="auth.isLoggedIn()">
                <a class="nav-link" (click)='toProfile()'>Profiel</a>
            </li>

            <li class='nav-item' *ngIf="auth.isLoggedIn() || chainAuth.isLoggedIn()">
                <a class="nav-link" (click)='logout()'  >Log uit</a>
            </li>
          </ul>
      </div>
    </nav>
  `,
  styles: [`
    a {
      margin: 5px;
    }

    .returnHome:hover {
      cursor: pointer;
    }

    input {
      padding: .275rem;
    }

    nav {
      border-bottom: 1px solid #dee2e6!important;
    }

  `]
})

// || !chainAuth.isLoggedIn()
export class NavbarComponent implements OnInit {
  public isHomepage:boolean;

  constructor(private router: Router, public auth: AuthService, public chainAuth: ChainAuthService) { }

  ngOnInit(): void {
  }

  logout(){
    this.auth.logout()
  }

  toHome() {
    this.router.navigate(['/']);
  }

  toCheckOut() {
    this.router.navigate(['/check-out/14']);
  }

  toLogin() {
    this.router.navigate(['gebruiker/login']);
  }

  toRegister() {
    this.router.navigate(['gebruiker/registreren']);
  }

  toProfile() {
    this.router.navigate(['profiel/p']);
  }

  toChainLogin() {
    this.router.navigate(['chain/login'])
  }

  toChainRegister() {
    this.router.navigate(['chain/register'])
  }

  toChainProfile() {
    this.router.navigate(["profiel-chain"])
  }

  toReviews(id = 14){
    this.router.navigate(["/reviews/" + id]);
  }

  toRestaurantMenu(id = 14){
    this.router.navigate(["/menu-chain/" + id]);
  }

  toRestaurants(code: String){
    code = code[0] + code[1];
    this.router.navigate(['restaurants', code])
  }

}
