/*
    Author: Joris Westera
    Version: 1.0
    Description: Container for order overview. Includes all functionality needed for the order overview container.
**/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/auth/users/auth.service';
import { DishesService, menuDetails, restaurantId } from "../../auth/dishes/dishes.service";
import { RestaurantServiceService} from '../../auth/restaurantService/restaurant-service.service';
import { CheckOutService} from '../../auth/check-out/check-out.service';


@Component({
  selector: 'app-order-overview-container',
  templateUrl: './order-overview-container.component.html',
  styleUrls: ['./order-overview-container.component.css'],
  providers: [CheckOutService]
})
export class OrderOverviewContainerComponent implements OnInit {
  order
  review
  orderDish = {}
  restaurant
  id: number;
  dish = []
  hasReview = false
  

  constructor(
    private checkOutService: CheckOutService,
    public auth: AuthService,
    private router: Router,
    private route: ActivatedRoute, 
    private dishService: DishesService,
    private restaurantService: RestaurantServiceService ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params =>{
      this.id = params["id"];
    });
    this.checkOutService.getOrder(this.id).subscribe(
      order => {
        this.order = order
        let temp = this.order.dishIds.replace(/[{("''")}]/g, '')
        this.orderDish = temp.split(",")
        for(let key in this.orderDish){
          let k = this.orderDish[key].split(":")
          this.dishService.getDish(k[0]).subscribe(
              dish => {
                this.dish.push(dish)
              },
              err =>{
                console.error(err)
              }
            )
        }
        this.restaurantService.getRestaurant(this.order.restaurantId).subscribe(
          restaurant => {
            this.restaurant = restaurant
          },
          err => {
            console.error(err);
          }
        )
      },
      err => {
        console.error(err);
      }
    )
    this.checkOutService.getReview(this.id).subscribe(
      review => {
        this.hasReview = true
        this.review = review
      },
      err =>{
        console.error(err)
      }
    )
  }
  getAantal(id){
    let temp = this.order.dishIds.replace(/[{("")}]/g, '')
    temp = temp.split(",")
    for(let t in temp){
      let x = this.orderDish[t].split(":")
      if(x[0] == id){
        return(x[1])
      }
    }
  }
  toHome(){
    this.router.navigate([''])
  }
  toReview(){
    this.router.navigate(['order-review/'+this.id])
  }
}
