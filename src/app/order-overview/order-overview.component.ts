/*
    Author: Joris Westera
    Version: 1.0
    Description: Component for order overview
**/

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-overview',
  templateUrl: './order-overview.component.html',
})
export class OrderOverviewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
