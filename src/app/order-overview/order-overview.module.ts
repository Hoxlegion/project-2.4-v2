/*
    Author: Joris Westera
    Version: 1.0
    Description: Model for order overview
**/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from '@angular/router';

import { OrderOverviewComponent } from './order-overview.component';
import { OrderOverviewContainerComponent } from './order-overview-container/order-overview-container.component';
import { CheckOutContainerComponent } from '../check-out/check-out-container/check-out-container.component';
import { ReviewPageComponent } from './review-page/review-page.component';

const loggerRoutes:Routes = [
  { path: '', component: OrderOverviewComponent, children: [
    {path: ':id', component: OrderOverviewContainerComponent},
    {path: 'order-review/:id', component: ReviewPageComponent}
  ]}
]

@NgModule({
  declarations: [
    OrderOverviewComponent,
    OrderOverviewContainerComponent,
    ReviewPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(loggerRoutes)
  ]
})
export class OrderOverviewModule { }