/*
    Author: Joris Westera
    Version: 1.0
    Description: Component for the review page. Includes all functionality for the review page
**/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService, TokenPayload } from '../../auth/users/auth.service';
import { DishesService, menuDetails, restaurantId } from "../../auth/dishes/dishes.service";
import { RestaurantServiceService} from '../../auth/restaurantService/restaurant-service.service';
import { CheckOutService, ReviewDetails} from '../../auth/check-out/check-out.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-review-page',
  templateUrl: './review-page.component.html',
  styleUrls: ['./review-page.component.css'],
  providers: [CheckOutService]
})
export class ReviewPageComponent implements OnInit {
  reviewInterface: ReviewDetails = {
    userId: 0,
    restaurantId: 0,
    title: '',
    description: '',
    rating: 0,
    orderId: ''
  }
  order
  review
  restaurant
  id: number
  hasReview = false
  token: TokenPayload

  reviewForm = new FormGroup({
    rating: new FormControl(),
    description: new FormControl(),
  })

  constructor(
    private fb:FormBuilder,
    private checkOutService: CheckOutService,
    private AuthService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private restaurantService: RestaurantServiceService ) {
      this.reviewForm = this.fb.group({
        rating: [''],
        description: ['', Validators.required],
        title: ['', Validators.required]
      })
    }

  ngOnInit(): void {
    this.token = this.AuthService.getUserDetails()
      if(this.token != null){
        this.reviewInterface.userId = this.token.id
      }
    this.reviewInterface.rating=5
    this.route.params.subscribe(params =>{
      this.id = params["id"];
    });
    this.checkOutService.getOrder(this.id).subscribe(
      order => {
        this.order = order
        this.restaurantService.getRestaurant(this.order.restaurantId).subscribe(
          restaurant => {
            this.restaurant = restaurant
          },
          err => {
            console.error(err);
          }
        )
      },
      err => {
        console.error(err);
      }
    )
    this.checkOutService.getReview(this.id).subscribe(
      review => {
        this.hasReview = true
        this.review = review
      },
      err =>{
        console.error(err)
      }
    )
  }
  postReview(){
    this.reviewInterface.orderId = this.order.id
    this.reviewInterface.restaurantId = this.restaurant.id
    this.checkOutService.setReview(this.reviewInterface).subscribe(
      id => {
        this.id=id
        this.router.navigate(['order-overview/'+ this.reviewInterface.orderId])
      })
  }
}
