/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: Component which contains the code for combining all the components relevant for the restaurant menu into one working page.
**/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-menu-container',
  templateUrl: './menu-container.component.html',
  styleUrls: ['./menu-container.component.css']
})
export class MenuContainerComponent{
  id: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute
    ) { }

    /**
     * Function which changes the current view to the shopping cart.
     */
  toCart() {
    this.route.params.subscribe(params =>{
      this.id = params["id"];
    });
    this.router.navigate(['/shopping-cart/' + this.id])
  }
}
