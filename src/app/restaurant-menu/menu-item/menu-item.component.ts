/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: Component which contains the code for the menu items which are generated based
    on the data from the back-end
**/

import { Component, OnInit } from '@angular/core';
import { DishesService, menuDetails, restaurantId } from "../../auth/dishes/dishes.service";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.css']
})

/**
 * @params menu       An interface which is used to store all the dishes that are returned by the back-end
 * @params id         An interface which contain an Id which will be used to look the dishes of a restaurant.
 * @params courses    An array of categories which is used to create categories
 * @params isLoaded   A boolean which determines whether a page is redy or not ready to load.
 */
export class MenuItemComponent implements OnInit {
  menu: menuDetails;
  id: restaurantId;
  courses = ["Voorgerecht", "Hoofdgerecht", "Drinken"]
  isLoaded = false;

  constructor(private dishService: DishesService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params["id"];
    });

    this.dishService.getMenu(this.id).subscribe(
      menu => {
        this.menu = menu;
        this.isLoaded = true
      },
      err => {
        console.error(err)
      }
    )

    localStorage.setItem("Order", JSON.stringify({}));
  }

  /**
   * Adds a dish with the specified amount to the localstorage so it can be used in the shoppig cart
   * @param id          Id of the dish
   * @param amountId    Amount that the user submitted
   */
  additem(id, amountId) {
    if(typeof amountId === "number"){
      let itemAmount = (document.getElementById(amountId.toString()) as HTMLInputElement).value;

      let temp = JSON.parse(localStorage.getItem("Order"));
      temp[id] = itemAmount;
      localStorage.setItem("Order", JSON.stringify(temp));
    }
    else{
      alert("Alleen gehele getallen mogen hier ingevuld worden")
    }
  }

  /**
   * A function which splits the arrays into an array so it can be used with *ngFor
   * @param allergies         A string wich contains the allergens a dish has.
   * @return allergies.split  An array which conains all allergies.
   */
  processAllergies(allergies: string){
    return allergies.split(",")
  }
}
