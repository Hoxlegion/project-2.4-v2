/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: Component which contains the code for the menu items which are generated based
    on the data from the back-end
**/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RestaurantServiceService, oneRestaurant } from 'src/app/auth/restaurantService/restaurant-service.service';

@Component({
  selector: 'app-restaurant-info',
  templateUrl: './restaurant-info.component.html',
  styleUrls: ['./restaurant-info.component.css']
})

/**
 * @params restaurantInfo    An interface which contains restarurantInfo
 * @params id                The id of the restaurant that is currently being looked at
 * @params isLoaded          A boolean which determines whether this component is ready or not ready to be loaded.
 */
export class RestaurantInfoComponent implements OnInit {
  restaurantInfo: oneRestaurant;
  id;
  isLoaded = false;

  constructor(private resService: RestaurantServiceService, private router: Router, private currentRoute: ActivatedRoute) { }

  /**
   * Fetches the id from the current restaurtant and stores it in id.
   * Alse requests restaurant data from the back-end.
   */
  ngOnInit(): void {
    this.currentRoute.params.subscribe(params => {
      this.id = params["id"]
    });

    this.resService.getRestaurant(this.id).subscribe(
      restaurant => {
        this.restaurantInfo = restaurant;
        this.isLoaded = true;
      },
      err => {
        console.error(err);
      }
    )
  }

  /**
   * Navigate to restaurant review page.
   */
  showReviews() {
    this.router.navigate(["reviews/" + this.id]);
  }
}
