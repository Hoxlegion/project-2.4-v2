/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: Module which contains the routing for the restaurant menu page/component.
**/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RestaurantInfoComponent } from './restaurant-info/restaurant-info.component';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { MenuContainerComponent } from './menu-container/menu-container.component';
import { FormsModule} from "@angular/forms";

import { Routes, RouterModule } from "@angular/router";
import { RestaurantMenuComponent } from './restaurant-menu.component';

const menuRoutes: Routes = [
  {
    path: "", component: RestaurantMenuComponent, children: [
      { path: ":id", component: MenuContainerComponent }
    ]
  }
]

@NgModule({
  declarations: [RestaurantMenuComponent, RestaurantInfoComponent, MenuItemComponent, MenuContainerComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(menuRoutes),
    FormsModule
  ]
})
export class RestaurantMenuModule { }
