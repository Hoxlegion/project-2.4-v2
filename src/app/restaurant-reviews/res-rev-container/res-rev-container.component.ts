/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: Component which combines the other review components to display a full review page.
**/

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-res-rev-container',
  templateUrl: './res-rev-container.component.html',
  styleUrls: ['./res-rev-container.component.css']
})
export class ResRevContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

  }
}
