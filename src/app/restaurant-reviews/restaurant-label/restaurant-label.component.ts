/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: Component which contains the code for getting and displaying restauranbt info on
    the reviews page.
**/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RestaurantServiceService, oneRestaurant } from 'src/app/auth/restaurantService/restaurant-service.service';

@Component({
  selector: 'app-restaurant-label',
  templateUrl: './restaurant-label.component.html',
  styleUrls: ['./restaurant-label.component.css']
})

/**
* @params restaurantId        A string which contains the restaurant id of the restaurant that the reviews are requested for
* @params currentRestaurant   An interface which contains information about the reviews of the restaurant that is currently being looked at.
* @params isLoaded            A boolean which affects whether a page is loaded or not loaded.
*/

export class RestaurantLabelComponent implements OnInit {

  constructor(private restaurantService: RestaurantServiceService, private router: Router, private currentRoute: ActivatedRoute) { }
  
  restaurantId: string;
  currentRestaurant: oneRestaurant;
  isLoaded = false;

  /**
  * Gets the id of the restaurant that is currently being looked at from the activeRoute.
  * Also requests the data about the restaurant based on the id from the back-end.
  */

  ngOnInit(): void {
    this.currentRoute.params.subscribe(params => {
      this.restaurantId = params["id"];
    });

    this.restaurantService.getRestaurant(this.restaurantId).subscribe(restaurant =>{
      this.currentRestaurant = restaurant;
      this.isLoaded = true;
    })
  }

  /*
  * Sends the user back to the page they came from.
  */
  backToMenu(){
    window.history.back();
  }

}