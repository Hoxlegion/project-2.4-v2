/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: Module which contains the routing for 
**/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RestaurantLabelComponent } from './restaurant-label/restaurant-label.component';
import { ReviewComponent } from './review/review.component';
import { ResRevContainerComponent } from './res-rev-container/res-rev-container.component';
import { RestaurantReviewComponent } from "./restaurant-reviews.component"
import { Routes, RouterModule } from "@angular/router";

const reviewRoutes: Routes = [
  {
    path: "", component: RestaurantReviewComponent, children: [
      { path: ":id", component: ResRevContainerComponent }
    ]
  }
]

@NgModule({
  declarations: [RestaurantLabelComponent, ReviewComponent, ResRevContainerComponent, RestaurantReviewComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(reviewRoutes)
  ]
})
export class RestaurantReviewsModule { }
