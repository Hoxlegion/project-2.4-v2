/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: Component which contains the code for getting data from the back-end 
    and displaying that data
**/

import { Component, OnInit } from '@angular/core';
import {reviews, ReviewAuthService} from "../../auth/reviews/reviews.service"
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})

/*
* @params currentRoutId   An number which contains the id of the restautaurant of which reviews should be displayed.
* @params resReviews      An interface which contains all the reviews from a specific restaurant.
* @params isLoaded        A boolean which enables/disables whether the pag is displayed.
**/
export class ReviewComponent implements OnInit {
  currentRouteId: number;
  resReviews: reviews;
  isLoaded = false;

  constructor(private reviewService: ReviewAuthService, private router: Router, private currentRoute: ActivatedRoute) { }

  /*
  * Sets currentRoutId based on the activeRoute. Then fetches all reviews based on currenRouteId.
  * Once this is completed, isLoaded becomes true and the page is loaded.
  **/
  ngOnInit(): void {
    this.currentRoute.params.subscribe(params => {
      this.currentRouteId = params["id"];
    })

    this.reviewService.getReviews(this.currentRouteId).subscribe(reviews => {
      this.resReviews = reviews;
      this.isLoaded = true;
    });
  }
}
