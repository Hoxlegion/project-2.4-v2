/*
    Author: Victor Vrancianu
    Version: 1.0
    Description:
**/


import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { RestaurantServiceService } from '../../auth/restaurantService/restaurant-service.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

/*  database: any = [
    {name: 'Hasret', beschrijving: 'Lekker eten', address: [93], voedsel: ['Alles', 'Patat', 'Pizza', 'Turks'], allergien: ['Koemelk']},
    {name: 'Subway', beschrijving: 'Lekker eten', address: [93, 10], voedsel: ['Alles', 'Turks'], allergien: ['Koemelk', 'Kippenei']},
    {name: 'Domino\'s Pizza', beschrijving: 'Lekker eten', address: [93, 10], voedsel: ['Alles', 'Pizza', 'Turks'], allergien: ['Kippenei']},
    {name: 'McDonald\'s', beschrijving: 'Lekker eten', address: [93], voedsel: ['Alles'], allergien: ['Koemelk', 'Pinda']},
    {name: 'Mr Sushi', beschrijving: 'Lekker eten', address: [93], voedsel: ['Alles'], allergien: ['Koemelk']},
  ];*/

  database: any;
  categorie: any;
  name: any;
  code: number;
  voedsel: any;
  currentAllergie: any;
  bezorgkosten: any
  check: boolean;

  constructor(private route: ActivatedRoute, private router: Router, private restaurantService: RestaurantServiceService) {
    this.voedsel = 0;
    this.check = true;
    this.currentAllergie = [''];
  }

  ngOnInit(): void {

    this.name = [];


    this.restaurantService.allRestaurants().subscribe(
      restaurants => {
        this.database = restaurants;
        this.parser()

      },
      err => {
        console.error(err)
      }
    )

    this.route.params.subscribe(params => {
      this.code = params.code;
    });



  }

  parser(){
   for (let data of this.database){
     for (let categorie of data.Restaurant_catagories){
       this.categorie = categorie.categorieId
     }
   }
  }

  toHome() {
    this.router.navigate(['/home']);
  }

  toRestaurant(id: any){
    this.router.navigate(['/menu/' + id]);
  }

  navigateToFoo(voedsel: any){

    this.voedsel = voedsel;

    this.ngOnInit();
  }

  reset(){
    this.check = true

    this.ngOnInit();
  }

  allergien(allergien: string){
    if (this.currentAllergie.includes('')){
      this.currentAllergie.splice(0, 1);
    }
    if (this.currentAllergie.includes(allergien)){
      const index = this.currentAllergie.indexOf(allergien);
      if (index > -1){
        this.currentAllergie.splice(index, 1);
      }
    } else{
      this.currentAllergie.push(allergien);
    }

    if (this.currentAllergie.length < 1){
      this.currentAllergie.push('');
    }
    this.ngOnInit();
  }

   test(prijs: any){
    if(this.check || this.bezorgkosten == prijs){
      return true
    }
    return false
   }


  prijs (prijs: any){
    this.bezorgkosten = prijs;
    this.check = false
    this.ngOnInit();
  }

  findCommonElements3(arr1, arr2) {
    return arr1.some(item => arr2.includes(item));
  }

  checkVoedsel(categorie: any, name: any){
    this.database = [];
    if (categorie == this.voedsel){
      return true
    }
    else if (this.voedsel == 0 && !this.name.includes(name)){
      this.name.push(name);
      return true
    }else{
      return false;
    }


  }

  checkAddress(address: any){
    address = address[0] + address[1]
    if (address == this.code){
      return true
    }
    return false
  }

}
