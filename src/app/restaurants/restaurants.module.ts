import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './filter/filter.component';
// import {RestaurantComponent} from '../restaurants/filter/filter.component';
import { Routes, RouterModule } from '@angular/router';

const restaurantsRoutes: Routes = [
    { path: '', component: FilterComponent }
];

@NgModule({
    declarations: [FilterComponent],
    exports: [
        FilterComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(restaurantsRoutes)
    ]
})
export class RestaurantsModule { }
