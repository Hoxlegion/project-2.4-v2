/*
    Author: Joris Westera
    Version: 1.0
    Description: Containter component for the shopping cart. Includes all functionality needed for shopping cart.
**/
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { DishesService, menuDetails, restaurantId } from "../../auth/dishes/dishes.service";
import { RestaurantServiceService, oneRestaurant } from '../../auth/restaurantService/restaurant-service.service';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-shopping-cart-container',
  templateUrl: './shopping-cart-container.component.html',
  styleUrls: ['./shopping-cart-container.component.css']
})
export class ShoppingCartContainerComponent implements OnInit {
  menu = []
  prices = {}
  price = 0
  id = 0
  restaurant: oneRestaurant
  isLoaded = false;
  deliveryCosts = 0

  constructor(private dishService: DishesService, private router: Router, private restaurantService: RestaurantServiceService) { }

  ngOnInit(): void {
    let temp = JSON.parse(localStorage.getItem("Order"));

    for(let key in temp){
      this.dishService.getDish(key).subscribe(
        menu => {
          this.menu.push(menu)
          this.id = this.menu[0].restaurantId
          this.restaurantService.getRestaurant(this.menu[0].restaurantId).subscribe(
            restaurant => {
              this.deliveryCosts = parseInt(restaurant.deliveryCosts)
              this.restaurant = restaurant;
             },
             err => {
               console.error(err);
             }
          )
        },
        err =>{
          console.error(err)
        }
      )
      this.isLoaded = true;
    }
  }

  getPrice(id, p){
    let temp = JSON.parse(localStorage.getItem("Order"));
    for(let key in temp){
      if(id == key){
        this.prices[key] = temp[key]*p
        this.price = 0
          for(let key in this.prices){
          this.price += this.prices[key]
        }
        return(temp[key]*p)
      }
    }
  }

  addItem(id) {
    let temp = JSON.parse(localStorage.getItem("Order"));
    temp[id] = parseInt(temp[id])+1;
    localStorage.setItem("Order", JSON.stringify(temp));
    localStorage.add
  }

  removeItem(id){
    let temp = JSON.parse(localStorage.getItem("Order"));
    if(temp[id]>0){
      temp[id] = temp[id]-1;
      localStorage.setItem("Order", JSON.stringify(temp));
      localStorage.add
    }
  }

  getTotal(id){
    let temp = JSON.parse(localStorage.getItem("Order"));
    return temp[id]
  }

  toMenu(){
    this.router.navigate(['menu/' + this.menu[0].restaurantId])
  }

  toCheckOut(){
    localStorage.setItem("cart", JSON.stringify(this.price));
    this.router.navigate(['check-out/' + this.menu[0].restaurantId])
  }
}
