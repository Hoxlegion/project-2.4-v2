/*
    Author: Joris Westera
    Version: 1.0
    Description: Model used for the shopping cart.
**/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from '@angular/router';

import { ShoppingCartComponent } from './shopping-cart.component';
import { ShoppingCartContainerComponent } from './shopping-cart-container/shopping-cart-container.component';

const loggerRoutes:Routes = [
  { path: ':id', component: ShoppingCartContainerComponent}
]


@NgModule({
  declarations: [
    ShoppingCartComponent,
    ShoppingCartContainerComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(loggerRoutes)
  ]
})
export class ShoppingCartModule { }
