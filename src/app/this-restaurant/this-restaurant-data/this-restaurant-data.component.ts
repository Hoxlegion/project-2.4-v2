/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { RestaurantServiceService, oneRestaurantUpdate } from '../../auth/restaurantService/restaurant-service.service'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-this-restaurant-data',
  templateUrl: './this-restaurant-data.component.html',
  styleUrls: ['./this-restaurant-data.component.css']
})
export class ThisRestaurantDataComponent implements OnInit {
  editingMode = true;
  logoToUpload: File;

  detailsToUpdate: oneRestaurantUpdate = {
    name: '',
    email: '',
    streetname: '',
    streetnumber: '',
    zipcode: '',
    city: '',
    phonenumber: '',
    minOrderPrice: 0,
    deliveryCosts: 0,
    deliveryZipcodes: '',
    categories: '',
    description: ''
  }

  form = new FormGroup ({
    name: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
    passwordRe: new FormControl(),
    streetname: new FormControl(),
    streetnumber: new FormControl(),
    zipcode: new FormControl(),
    city: new FormControl(),
    phonenumber: new FormControl(),
    minOrderPrice: new FormControl(),
    deliveryCosts: new FormControl(),
    deliveryZipcodes: new FormControl(),
    categories: new FormControl(),
    description: new FormControl()
  })

  constructor(
    private fb:FormBuilder,
    public resService: RestaurantServiceService,
    public router: Router,
    private currentRoute: ActivatedRoute
    ) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      passwordRe: ['', Validators.required],
      streetname: ['', Validators.required],
      streetnumber: ['', Validators.required],
      zipcode: ['', Validators.required],
      city: ['', Validators.required],
      phonenumber: ['', Validators.required],
      minOrderPrice: ['', Validators.required],
      deliveryCosts: ['', Validators.required],
      deliveryZipcodes: ['', Validators.required],
      categories: ['', Validators.required],
      description: ['', Validators.required]
    })
   }

  restaurantData:any
  restaurantId:any

  ngOnInit(): void {
    this.currentRoute.params.subscribe(params => {
      this.restaurantId= params['id'];
    })

    this.resService.getRestaurant(this.restaurantId).subscribe(
      restaurant => {
        this.restaurantData = restaurant
        this.setRestaurantDetails(this.restaurantData)
      }
    )
  }

  enableEditing() {
    this.editingMode = !this.editingMode;
  }

  setRestaurantDetails(details) {
    this.form.patchValue({
      name: details.name,
      email: details.email,
      password: details.password,
      streetname: details.streetname,
      streetnumber: details.streetnumber,
      zipcode: details.zipcode,
      city: details.city,
      phonenumber: details.phonenumber,
      minOrderPrice: details.minOrderPrice,
      delivertyCosts: details.delivertyCosts,
      deliveryZipcodes: details.deliveryZipcodes,
      categories: details.categories,
      description: details.description
    })
  }

  updateRestaurantInfo() {
    this.resService.updateRestaurant(this.detailsToUpdate).subscribe(
      () => {
        this.editingMode = !this.editingMode
      },
      err => {
        console.log(err)
      }
    )
  }

}
