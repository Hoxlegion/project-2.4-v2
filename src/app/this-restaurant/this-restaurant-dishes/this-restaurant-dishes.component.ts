/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component, OnInit } from '@angular/core';
import { DishesService, menuAddDetails, restaurantId } from "../../auth/dishes/dishes.service";
import { Router, ActivatedRoute } from "@angular/router";
import { RestaurantServiceService, oneRestaurant } from 'src/app/auth/restaurantService/restaurant-service.service';

@Component({
  selector: 'app-this-restaurant-dishes',
  templateUrl: './this-restaurant-dishes.component.html',
  styleUrls: ['./this-restaurant-dishes.component.css']
})
export class ThisRestaurantDishesComponent implements OnInit {
  id: any;
  status: boolean = true;
  menu: menuAddDetails;
  restaurantInfo: oneRestaurant;
  isLoaded = false;

  constructor(private resService: RestaurantServiceService, private dishService: DishesService, private router: Router, private currentRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.currentRoute.params.subscribe(params =>{
      this.id = params["id"];
    });

    this.resService.getRestaurant(this.id).subscribe(
      restaurant => {
        this.restaurantInfo = restaurant;
        this.isLoaded = true
       },
       err => {
         console.error(err);
       }
    )

    this.dishService.getMenu(this.id).subscribe(
      menu => {
        this.menu = menu;
      },
      err =>{
        console.error(err)
      }
    )

      localStorage.setItem("Order", JSON.stringify({}));
  }

  fooddetails() {
    this.status = !this.status
  }

  additem(id, amountId) {
    let itemAmount = (document.getElementById(amountId) as HTMLInputElement).value;
    
    let temp = JSON.parse(localStorage.getItem("Order"));
    temp[id] = itemAmount;
    localStorage.setItem("Order", JSON.stringify(temp));

    localStorage.add
  }

  showReviews(){
    this.router.navigate(["reviews/" + this.id]);
  }

  toAddDish() {
    this.router.navigate(["add/" + this.id]);
  }

  toUpdateDish(id) {
    this.router.navigate(["update/" + id])
  }

  processAllergies(Allergieen){
    return Allergieen.split(",")
  }

  dishDelete(dishId: number){
    this.dishService.deleteDish(dishId).subscribe(
      deletedDish => {
      console.log("test")
    },
    err => {
      console.log(err.error.text.substr(-12))
      if(err.error.text.substr(-11) == "was removed"){
        location.reload();
      }
      else{
        console.error(err)
      }
    }
    )
  }
}
