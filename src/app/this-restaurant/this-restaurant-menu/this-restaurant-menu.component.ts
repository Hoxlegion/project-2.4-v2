/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-this-restaurant-menu',
  templateUrl: './this-restaurant-menu.component.html',
  styleUrls: ['./this-restaurant-menu.component.css']
})
export class ThisRestaurantMenuComponent implements OnInit {
  restaurantId:any
  private routeSub: Subscription;

  constructor(private currentRoute: ActivatedRoute, public router: Router) {
    
    this.routeSub = this.currentRoute.params.subscribe(params => {
      this.restaurantId= params['id'];
    })
  }

  ngOnInit(): void {
    
  }

  toMyData() {
    this.router.navigate(['/dit-restaurant/gegevens/', this.restaurantId])
  }

  toMyDishes() {
    this.router.navigate(['/dit-restaurant/menu-chain/', this.restaurantId])
  }

  toMyOrders() {
    this.router.navigate(['/dit-restaurant/bestellingen/', this.restaurantId])
  }

}
