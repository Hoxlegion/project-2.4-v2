/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DishesService, menuAddDetails } from '../../auth/dishes/dishes.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-this-restaurant-new-dish',
  templateUrl: './this-restaurant-new-dish.component.html',
  styleUrls: ['./this-restaurant-new-dish.component.css']
})
export class ThisRestaurantNewDishComponent implements OnInit {
  id:any

  credentials: menuAddDetails = {
    id: 0,
    restaurantId: 0,
    name: '',
    description: '',
    ingredients: '',
    price: 0,
    allergies: "",
    course: ""
  }

  form = new FormGroup ({
    name: new FormControl(),
    description: new FormControl(),
    ingredients: new FormControl(),
    price: new FormControl(),
    allergies: new FormControl(),
    course: new FormControl()
  })

  constructor(public dishService: DishesService, public router: Router, public currentRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.currentRoute.params.subscribe(params => {
      this.credentials.restaurantId = params['id']
      this.id = params['id']
    })
  }

  addDish() {
    this.dishService.addDish(this.credentials).subscribe(
      () => {
        this.router.navigate(['dit-restaurant/menu-chain/' + this.id])
      },
      err => {
        console.error(err)
      }
    )
  }
}
