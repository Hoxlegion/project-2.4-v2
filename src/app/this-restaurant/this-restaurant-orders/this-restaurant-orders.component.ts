/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CheckOutService, checkOutDetails, updateDelivered } from '../../auth/check-out/check-out.service';

@Component({
  selector: 'app-this-restaurant-orders',
  templateUrl: './this-restaurant-orders.component.html',
  styleUrls: ['./this-restaurant-orders.component.css'],
  providers: [CheckOutService]
})
export class ThisRestaurantOrdersComponent implements OnInit {
  restaurantId:any
  currentOrders:checkOutDetails
  isLoaded = false

  detailsToUpdate: updateDelivered = {
    id: 0,
    isDelivered: 0
  }

  constructor(private checkOutService: CheckOutService, private router: Router, private currentRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.currentRoute.params.subscribe(params => {
      this.restaurantId = params["id"];
    });

    this.checkOutService.getRestaurantOrders(this.restaurantId).subscribe(orders => {
      this.currentOrders = orders;
      this.isLoaded = true;
    }),
    err => {
      console.error(err);
    }

  }
  
  checkIfSend(code) {
    return code == 0
  }

  /**
   * Function for starting the update of an orders information.
   */
  updateDeliveredId(id) {
    this.detailsToUpdate.isDelivered = 1
    this.detailsToUpdate.id = id
    
    this.checkOutService.updateOrderDelivered(this.detailsToUpdate).subscribe(
      () => {
        this.router.navigate(["bestellingen/", this.restaurantId])
      },
      err => {
        console.log(err)
      }
    )
  }


}
