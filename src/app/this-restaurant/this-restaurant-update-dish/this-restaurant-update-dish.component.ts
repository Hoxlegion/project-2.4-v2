/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { menuAddDetails, DishesService } from "../../auth/dishes/dishes.service"

@Component({
  selector: 'app-this-restaurant-update-dish',
  templateUrl: './this-restaurant-update-dish.component.html',
  styleUrls: ['./this-restaurant-update-dish.component.css']
})
export class ThisRestaurantUpdateDishComponent implements OnInit {
  dishId:any

  details: menuAddDetails = {
    id: 0,
    restaurantId: 0,
    name: '',
    description: '',
    ingredients: '',
    price: 0,
    allergies: "",
    course: ""
  }

  form = new FormGroup ({
    name: new FormControl(),
    description: new FormControl(),
    ingredients: new FormControl(),
    price: new FormControl(),
    allergies: new FormControl(),
    course: new FormControl()
  })

  constructor(public currentRoute: ActivatedRoute, private fb:FormBuilder, private dishService: DishesService, private router: Router, ) {

    this.form = this.fb.group({
      name: ['', Validators.required],
      description: [''],
      ingredients: ['', Validators.required],
      price: ['', Validators.required],
      allergies: [""],
      course: ["", Validators.required]
    })
  }

  ngOnInit(): void {
    this.currentRoute.params.subscribe(params => {
      this.details.id = params['id']
    })

    this.dishService.getDish(this.details.id).subscribe(
      dish => {
        this.details = dish;
        this.setDishDetails(this.details);
      },
      err => {
        console.error(err)
      }
    )
  }


  setDishDetails(details){
    this.form.patchValue({
      name: details.name,
      description: details.description,
      ingredients: details.ingredients,
      price: details.price,
    });
  }

  /**
   * Function for starting the update of a dish information.
   */
  updateDish() {
    this.dishService.updateDish(this.details).subscribe(
      () => {
        this.router.navigate(["dit-restaurant/menu-chain/" + this.details.restaurantId])
      },
      err => {
        console.log(err)
      }
    )
  }

}
