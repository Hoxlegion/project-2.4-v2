/*
    Author: Timo de Jong
    Version: 1.0
    Description: 
**/

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-this-restaurant',
  templateUrl: './this-restaurant.component.html'
})
export class ThisRestaurantComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
