import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThisRestaurantComponent } from './this-restaurant.component';
import { Routes, RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ThisRestaurantMenuComponent } from "./this-restaurant-menu/this-restaurant-menu.component";
import { ThisRestaurantDataComponent } from './this-restaurant-data/this-restaurant-data.component';
import { ThisRestaurantOrdersComponent } from './this-restaurant-orders/this-restaurant-orders.component';
import { ThisRestaurantDishesComponent } from './this-restaurant-dishes/this-restaurant-dishes.component';
import { ThisRestaurantNewDishComponent } from './this-restaurant-new-dish/this-restaurant-new-dish.component';
import { ThisRestaurantUpdateDishComponent } from './this-restaurant-update-dish/this-restaurant-update-dish.component';

const thisRestaurantRoutes: Routes = [
  {
    path: "", component: ThisRestaurantComponent, children: [
      { path: ":id", component: ThisRestaurantMenuComponent },
      { path: "gegevens/:id", component: ThisRestaurantDataComponent },
      { path: "bestellingen/:id", component: ThisRestaurantOrdersComponent },
      { path: "menu-chain/:id", component: ThisRestaurantDishesComponent },
      { path: "add/:id", component: ThisRestaurantNewDishComponent },
      { path: "update/:id", component: ThisRestaurantUpdateDishComponent }
    ]
  }
]

@NgModule({
  declarations: [
    ThisRestaurantComponent, 
    ThisRestaurantMenuComponent, 
    ThisRestaurantDataComponent, 
    ThisRestaurantOrdersComponent, 
    ThisRestaurantDishesComponent, 
    ThisRestaurantNewDishComponent, 
    ThisRestaurantUpdateDishComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(thisRestaurantRoutes)
  ]
})
export class ThisRestaurantModule { }

