/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: Component which displays the users saved locations.
**/

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.css']
})


/*
* @param  street           Contains the street of a user
* @param  houseNumber      Contains the house number of a user
* @param  postalCode       Contains the postalcode of a user
**/
export class LocationsComponent implements OnInit {
  street;
  houseNumber;
  postalCode;

  constructor() { }

  ngOnInit(): void {
    this.street = "Piet Heijn Straat"
    this.houseNumber = "1577"
    this.postalCode = "1234AG"
  }
}