/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: Component which contains the code for changing a users password when
    he/she is logged in.
**/

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService, passwordUpdate } from 'src/app/auth/users/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})


/*
* @params passwordToUpdate    An interace of type passwordUpdate used for sending data to the back-end
* @params passwordForm        An formGroup(), used for binding values to to the form-group in the HTML
**/
export class PasswordComponent implements OnInit {
  passwordToUpdate: passwordUpdate;

  passwordForm = new FormGroup({
    curPassword: new FormControl(),
    newPassOne: new FormControl(),
    newPassTwo: new FormControl()
  })

  passToUpdate: passwordUpdate = {
    password: "",
    currentPassword: ""
  }

  constructor(private authService: AuthService, private router: Router, private fb: FormBuilder) {

    this.passwordForm = this.fb.group({
      curPassword: ["", Validators.required],
      newPassOne: ["", Validators.required],
      newPassTwo: ["", Validators.required]
    })
  }

  ngOnInit(): void {
  }

  
/*
*  Function which check whether all fields are filled in, if they are: send data to back-end for updating.
*  If not all fields are filled in display an alert with relevant info.
**/
  updatePassword() {
    if(this.passwordForm.controls["newPassOne"].valid && this.passwordForm.controls["newPassTwo"].valid && this.passwordForm.controls["curPassword"].valid){
      if(this.passwordForm.controls["newPassOne"].value == this.passwordForm.controls["newPassTwo"].value){
        this.authService.updatePassword(this.passToUpdate).subscribe(
          () => {
            alert("Wachtwoord aangepast, u wordt uitgelogd")
            this.authService.logout();
          },
          err => {
            if(err.error.text =="User was updated"){
              alert("Wachtwoord aangepast, u wordt uitgelogd")
              this.authService.logout();
            }
            else if(err.error.text =="Current password does not match"){
              alert("Huidig wachtwoord is incorrect")
            }
            else{
              console.log(err)
            }
          }
        )
      }
    }
    else{
      alert("Niet alle velden zijn in gevuld")
    }
  }
}
