/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: Component which contains all the other component's so that they can be
    displayed on one page.
**/

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-info-container',
  templateUrl: './user-info-container.component.html',
  styleUrls: ['./user-info-container.component.css']
})
export class UserInfoContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
