/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: Module which contains the routing for all components that display the user related data.
**/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { Routes, RouterModule } from '@angular/router';
import { LocationsComponent } from './locations/locations.component';
import { UserInfoContainerComponent } from './user-info-container/user-info-container.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { PasswordComponent } from './password/password.component';
import { UserOrdersComponent } from './user-orders/user-orders.component';
import { UserMenuComponent } from './user-menu/user-menu.component';


const profileRoutes: Routes = [
  { path: 'p', component: UserMenuComponent },
  { path: 'p/profiel', component: UserInfoContainerComponent },
  { path: 'p/orders', component: UserOrdersComponent}
]

@NgModule({
  declarations: [
    UserProfileComponent,
    LocationsComponent,
    UserInfoContainerComponent,
    PasswordComponent,
    UserOrdersComponent,
    UserMenuComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(profileRoutes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class UserInfoModule { }
