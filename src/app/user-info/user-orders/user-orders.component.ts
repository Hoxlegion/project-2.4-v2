import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CheckOutService, checkOutDetails } from '../../auth/check-out/check-out.service';
import { AuthService } from "../../auth/users/auth.service"

@Component({
  selector: 'app-user-orders',
  templateUrl: './user-orders.component.html',
  styleUrls: ['./user-orders.component.css'],
  providers: [CheckOutService]
})
export class UserOrdersComponent implements OnInit {
  userId:any
  currentOrders:checkOutDetails
  isLoaded = false

  constructor(private authService: AuthService, private checkOutService: CheckOutService, private router: Router) { }

  ngOnInit(): void {
    this.userId = this.authService.getUserDetails().id

    this.checkOutService.getUserOrders(this.userId).subscribe(orders => {
      this.currentOrders = orders;
      this.isLoaded = true;
    }),
    err => {
      console.error(err);
    }
  }

  checkIfSend(code) {
    return code == 1
  }

}
