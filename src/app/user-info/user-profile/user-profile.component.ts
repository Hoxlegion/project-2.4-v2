/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: A component which, when displayed, shows the information of a user.
    And make it possible for a user to edit their data.
**/

import { Component, OnInit } from '@angular/core';
import { AuthService, UserDetails, UserDetailsUpdate } from '../../auth/users/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})

/*
* @params details           An interface based on UserDetils. Used for receiving data from the back-end
* @params editingMode       A boolean, used to determine whether userForm can be edited.
* @params userForm          A FormGroup used for setting values to a form
* @params detailsToUpdate   
**/
export class UserProfileComponent implements OnInit {
  details: UserDetails
  editingMode = true;
  userForm = new FormGroup({
    fname: new FormControl(),
    lname: new FormControl(),
    dateofbirth: new FormControl(),
    email: new FormControl()
  })

  detailsToUpdate: UserDetailsUpdate = {
    first_name: "",
    last_name: "",
    email: "",
    birth_date: new Date,
  }

  constructor(private authService: AuthService, private router: Router, private fb: FormBuilder) {

    this.userForm = this.fb.group({
      fname: ["", Validators.required],
      lname: ["", Validators.required],
      dateofbirth: ["", Validators.required],
      email: ["", Validators.required]//
    })
  }

  ngOnInit() {
    this.authService.profile().subscribe(
      user => {
        this.details = user;
        this.setUserDetails(this.details);
      },
      err => {
        console.error(err)
      }
    )
  }

  /*
  * Places the received user data values into the form so they are displayed.
  */
  setUserDetails(details) {
    this.userForm.patchValue({
      fname: this.details.first_name,
      lname: this.details.last_name,
      dateofbirth: this.details.birth_date,
      email: this.details.email,
    });
  }

  /*
  * Makes it possible for a user to edit his user data.
  */
  enableEditing() {
    this.editingMode = !this.editingMode;
  }

  /*
  *Checks wherether all fields are valid, if they are. User data is updated.
  If ther are not, an alert with relevant info is displayed.
  */
  updateUserData() {
    if (this.userForm.controls["fname"].valid && this.userForm.controls["lname"].valid && this.userForm.controls["dateofbirth"].valid && this.userForm.controls["email"].valid) {
      this.authService.updateProfile(this.detailsToUpdate).subscribe(
        () => {
          alert("Wijziging geslaagd");
          this.authService.logout();
        },
        err => {
          if (err.error.text == "User was updated") {
            alert("Wijziging geslaagd");
            location.reload();
          }
          else {
            console.log(err)
          }
        })
    }
    else {
      alert("Niet alle velden zijn ingevuld")
    }
  }
}
