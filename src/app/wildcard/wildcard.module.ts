/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: A module which contains the component for the 404 page not found page.
**/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WildcardComponent } from './wildcard/wildcard.component';


@NgModule({
  declarations: [WildcardComponent],
  imports: [
    CommonModule
  ]
})
export class WildcardModule { }
