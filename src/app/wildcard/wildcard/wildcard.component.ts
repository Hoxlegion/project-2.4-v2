/*
    Author: Casper Scholte-Albers
    Version: 1.0
    Description: A component which will be displayed when a user navigates to
    a page that does not exist (404 Page not found)
**/


import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-wildcard',
  templateUrl: './wildcard.component.html',
  styleUrls: ['./wildcard.component.css']
})
export class WildcardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
